import numpy as np
import pandas as pd
import scipy.stats as stats


print("parts (a) and (b)")
pd.set_option("display.precision", 4)

n = 1000
df_i = pd.read_csv('buffon_needle_data.csv', comment='#')

dfs = []
for L in [0.5, 0.7]:
    alpha = 0.05
    z = stats.norm.ppf(1 - alpha / 2)

    Bn = df_i["L={:0.1f}".format(L)].values
    pn = Bn / n
    variance = pn * (1 - pn)
    #   g(pn)  =  2 * L / pn
    #  g'(pn)  = -2 * L / pn^2
    # |g'(pn)| =  2 * L / pn^2
    g = 2 * L / pn # i.e., pi
    gp = -2 * L / np.square(pn)

    lb = g - np.sqrt(variance) * abs(gp) * z / np.sqrt(n)
    ub = g + np.sqrt(variance) * abs(gp) * z / np.sqrt(n)

    df_o = pd.DataFrame()
    df_o['$\\hat{p}(n)$'] = pn
    df_o['$\\hat{\\pi}$'] = g
    df_o['95% CI (lo)'] = lb
    df_o['95% CI (hi)'] = ub
    dfs.append(df_o)

df_master = pd.concat(dfs, axis=1)
print(df_master)
df_master.to_csv('data2.csv')
df_master.to_latex('data2.tex')


print("part (c)")
# not sure about any of part (c)
L = 0.5
pn = 2 * L / np.pi
z = stats.norm.ppf(1 - alpha / 2)
variance = pn * (1 - pn)
gp = -2 * L / np.square(pn)
epsilon = 0.0001 / 2

n = np.square(z * abs(gp) / epsilon) * variance
print(n)

print("part (d)")
n0 = 100
Bn0 = 34
X = Bn0 / n0
S = X * (1 - X)
eps = 0.1

n = 2
while n < stats.t.ppf(0.975, df=n-1) ** 2 * S / X ** 6 / eps ** 2:
    n += 1
Ne = n
print(Ne)

BNe = 17927
X = BNe / Ne
S = X * (1 - X)

print(1 / X)
print(1 / X - stats.t.ppf(0.975, df=BNe-1) * np.sqrt(S) / X ** 2 / np.sqrt(Ne))
print(1 / X + stats.t.ppf(0.975, df=BNe-1) * np.sqrt(S) / X ** 2 / np.sqrt(Ne))
