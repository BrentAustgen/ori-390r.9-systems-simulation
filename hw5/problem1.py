import numpy as np
import pandas as pd
from tqdm import tqdm # progress bars

np.random.seed(0)

def gen_vars_until(threshold):
    x = list()
    x.append(np.random.normal())
    while np.std(x) / np.sqrt(len(x)) > threshold or len(x) < 30:
        x.append(np.random.normal())
    return x

dfs = []
N = 20
for idx, epislon in enumerate([0.1, 0.01], start=1):
    df = pd.DataFrame(columns=['n', 'avg.', 'var.'])
    for i in tqdm(range(N)):
        x = gen_vars_until(epislon)
        df.loc[i] = [len(x), np.mean(x), np.var(x)]
    dfs.append(df)

df_master = pd.concat(dfs, axis=1)
df_master.to_csv('data1.csv')
df_master.to_latex('data1.tex')
