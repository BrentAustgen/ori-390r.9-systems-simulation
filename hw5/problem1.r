epsilon = 0.1
num_trials = 20
min_n = 30
trial = 1

while (trial <= num_trials)
{
    X<-data.frame()
    i = 1
    while (i < min_n || sd(X[,1]) / sqrt(i - 1) > epsilon)
    {
        X[i,1] = rnorm(1)
        i = i + 1
    }
    cat(sprintf("trial: %d\n", trial))
    cat(sprintf("  n: %d\n", dim.data.frame(X)[1]))
    cat(sprintf("  avg: %f\n", mean(X[,1])))
    cat(sprintf("  var: %f\n", var(X[,1])))
    trial = trial + 1
}
