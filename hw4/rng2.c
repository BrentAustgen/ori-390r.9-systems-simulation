#include "RngStream.h"
#include <stdio.h>

int main() {

    RngStream g = RngStream_CreateStream("");
    int i;
    double res;
    for (i = 0; i < 32768 * 3; i++) {
        res = RngStream_RandU01(g);
        printf("%0.20f\n", res);
    }
    return 0;
}
