"""
Generates random numbers using the MRG32k3a method.
"""

import sys
from signal import signal, SIGPIPE, SIG_DFL


def main(num):
    """
    Sets up the RNG and writes [num] variates to STDOUT.
    """

    norm = 2.328306549295728e-10
    bigprime1 = 4294967087
    bigprime2 = 4294944443

    a1 = [810728, 1403580, 0]
    a2 = [1370589, 0, 527612]

    seed1 = [12345, 12345, 12345]
    seed2 = [12345, 12345, 12345]

    for _ in range(num):

        # Component 1
        comp1 = (a1[1] * seed1[1] - a1[0] * seed1[0]) % bigprime1
        seed1 = seed1[1], seed1[2], comp1

        # Component 2
        comp2 = (a2[2] * seed2[2] - a2[0] * seed2[0]) % bigprime2
        seed2 = seed2[1], seed2[2], comp2

        # Combination
        res = (comp1 - comp2 + bigprime1 * (comp1 <= comp2)) * norm
        sys.stdout.write("{:0.20f}\n".format(res))

if __name__ == '__main__':
    signal(SIGPIPE, SIG_DFL)
    try:
        main(int(sys.argv[1]))
    except IndexError:
        print("Usage: python3 rng2.py [num]")
        sys.exit()
