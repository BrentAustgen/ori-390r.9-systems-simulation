import numpy as np
import pandas as pd
import scipy.stats as stats
from collections import defaultdict

"""
Notes:
  - stats.<dist>.sf
    - survival function
    - used to get p-values from statistics
  - stats.<dist>.ppf
    - percent point function
    - used to get statistics from p-values
"""

class TestResult(object):
    
    def __init__(self, name, stat, pval, passfail):
        self.name = name
        self.stat = stat
        self.pval = pval
        self.passfail = passfail

    def display(self):
        print('  ' + self.name + ':')
        print('    statistic: {}'.format(self.stat))
        print('    p-value: {:1.4f} ({:s})'.format(self.pval, self.passfail))


U01 = [0, 1]
ALPHA = 0.05

RUNS_A = {
    (1, 1): 4529.4,
    (1, 2): 9044.9,
    (1, 3): 13568,
    (1, 4): 18091,
    (1, 5): 22615,
    (1, 6): 27892,
    (2, 2): 18097,
    (2, 3): 27139,
    (2, 4): 36187,
    (2, 5): 45234,
    (2, 6): 55789,
    (3, 3): 40721,
    (3, 4): 54281,
    (3, 5): 67852,
    (3, 6): 83685,
    (4, 4): 72414,
    (4, 5): 90470,
    (4, 6): 111580,
    (5, 5): 113262,
    (5, 6): 139476,
    (6, 6): 172860
}

RUNS_B = {
    1: 1. / 6,
    2: 5. / 24,
    3: 11. / 120,
    4: 19. / 720,
    5: 29. / 5040,
    6: 1. / 840
}

def chi_square(data, k=2 ** 12):
    """"
    Applies the chi-square uniformity test to the given data. Note that the
    null hypothesis is that the the data are uniformly distributed.
    """
    n = len(data)
    # NOTE: len(bin_edges) = len(res) + 1
    hist, bin_edges = np.histogram(data, bins=k, range=U01)
    scale = 1. * k / n
    stat = scale * np.square(hist - 1. / scale).sum()
    pval = stats.chi2.sf(stat, df=k-1)
    passfail = "FAIL" if pval <= ALPHA else "PASS"
    return TestResult("Chi Square (k={})".format(k),
                  stat, pval, passfail)

def kolmogorov_smirnov(data):
    """"
    Applies the Kolmogorov-Smirnov frequency test to the given data. Note that
    the null hypothesis is that the data come from the expected CDF.
    """
    sdata = sorted(data)
    n = len(data)
    Dn_pos = max(i / n - v for i, v in enumerate(sdata))
    Dn_neg = max(v - (i - 1) / n for i, v in enumerate(sdata))
    # TODO: address the weird multiplicative scaling term from class
    stat = max(Dn_pos, Dn_neg) *\
                 (np.sqrt(n) + 0.12 + 0.11 / np.sqrt(n))
    # validate stat value with stats.kstest(data, "uniform")
    pval = stats.kstwobign.sf(stat)
    passfail = "FAIL" if pval <= ALPHA else "PASS"
    return TestResult("K-S", stat, pval, passfail)


def serial_test(data, d=2, k=64):
    """
    Applies the serial test for independence to the given data. Note that the
    hypothesis is that the data are not independent.
    """
    n = len(data)
    rdata = data.reshape(int(n / d), d)
    hist, edges = np.histogramdd(rdata, bins=[k] * d, range=[U01] * d)
    scale = 1. * (k ** d) / (n / d)
    stat = scale * np.square(hist - 1. / scale).sum()
    pval = stats.chi2.sf(stat, df=k**d-1)
    passfail = "FAIL" if pval <= ALPHA else "PASS"
    return TestResult("Serial Test (d={}, k={})".format(d, k),
                      stat, pval, passfail)

def runs_test(data, ascending=True):
    """
    Applies the runs (up) test for independence to the given data. The null
    hypothesis of this test is that the data are not independent.
    """
    ctr = defaultdict(lambda: 0)
    cnt = 0
    last = None
    for x in data:
        if last and ((x < last and ascending) or (x > last and not ascending)):
            ctr[cnt] += 1
            cnt = 0
        cnt += 1
        last = x
    ctr[cnt] += 1

    # sanity check
    assert(sum(i * v for i, v in ctr.items()) == len(data))

    # truncate down to runs of 1, ..., 5, or 6 or more
    for key in list(filter(lambda x: x > 6, ctr)):
        ctr[6] += ctr.pop(key)
    n = len(data)
    Rn = 1. / n * sum(RUNS_A[(min(i, j), max(i, j))] *\
                      (ctr[i] - n * RUNS_B[i]) * (ctr[j] - n * RUNS_B[j])
                      for i in range(1, 6 + 1) for j in range(1, 6 + 1))
    pval = stats.chi2.sf(Rn, df=6)
    passfail = "FAIL" if pval <= ALPHA else "PASS"
    return TestResult("Runs Test", Rn, pval, passfail)

def autocorrelation_test(data, j=1, o=0):
    # TODO: remove or fix offset
    assert(o < j)
    n = len(data)
    h = int(np.floor((n - 1) / j) - 1)
    corr_j = -3
    for k in range(0, h + 1):
        corr_j += 12 / (h + 1) *\
                  data[o + k * j] * data[o + (k + 1) * j]

    variance = (13 * h + 7) / np.square(h + 1)
    Aj = corr_j / np.sqrt(variance)
    pval = stats.norm.sf(Aj)
    passfail = "PASS" if ALPHA / 2 < pval < 1 - ALPHA / 2 else "FAIL"
    return TestResult("Autocorrelation (j={}, o={})".format(j, o),
                      Aj, pval, passfail)



if __name__ == '__main__':

    rng1 = np.array(pd.read_csv('results/rng1.csv', header=None)[0])
    rng2 = np.array(pd.read_csv('results/rng2.csv', header=None)[0])

    for data in [rng1, rng2]:
        results = [
            chi_square(data[:32768]),
            kolmogorov_smirnov(data[:32768]),
            serial_test(data[:32768 * 2], k=64, d=2),
            serial_test(data[:32768 * 3], k=16, d=3),
            runs_test(data[:5000], ascending=True),
            autocorrelation_test(data[:5000], j=1),
            autocorrelation_test(data[:5000], j=2),
            autocorrelation_test(data[:5000], j=3),
            autocorrelation_test(data[:5000], j=4),
            autocorrelation_test(data[:5000], j=5),
            autocorrelation_test(data[:5000], j=6)
        ]
        for result in results:
            result.display()
