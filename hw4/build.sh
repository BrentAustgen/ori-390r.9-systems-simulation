#!/bin/bash

# === STREAM GENERATOR =======================================================

# check for files, retrieve if necessary
if [ ! -f RngStream.c ]
then
    wget -q http://www.iro.umontreal.ca/~lecuyer/myftp/streams00/c/RngStream.c
fi

if [ ! -f RngStream.h ]
then
    wget -q http://www.iro.umontreal.ca/~lecuyer/myftp/streams00/c/RngStream.h
fi

# build
gcc -Wall -c RngStream.c -o RngStream.o
gcc -Wall -c rng2.c -o rng2.o
gcc rng2.o RngStream.o RngStream.h -o rng2



# === STANDALONE GENERATOR ===================================================

if [ ! -f combmrg2.c ]
then
    wget -q http://www.iro.umontreal.ca/~lecuyer/myftp/papers/combmrg2.c
fi

# make additions to get it to build
cat >rng3.c <<EOF
#include <stdio.h>
EOF

sed -e '/#####/,$d' combmrg2.c >>rng3.c

cat >>rng3.c <<EOF
int main()
{
    s10 = s11 = s12 = s20 = s21 = s22 = 12345;

    double res;
    int i;
    for (i = 0; i < 32768 * 3; i++)
    {
        res = MRG32k3a();
        printf("%0.20f\n", res);
    }
    return 0;
}
EOF

# build
gcc -Wall rng3.c -o rng3
