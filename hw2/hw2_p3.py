from functools import partial

import numpy as np
import scipy.stats as stats

seed = 0
np.random.seed(seed)
subproblem = 'a' # or 'c' to use distribution variances in CI computations
alpha = 0.05

sample_sizes = [5, 10, 25, 100]
trials = 1000

quantiles = ['Z', 'T']
distributions = {
    'normal':
        { 'function': partial(np.random.normal, *[0, 1]),
          'mean': 0,
          'variance': 1 },
    'exponential':
        { 'function': partial(np.random.exponential, *[1]),
          'mean': 1,
          'variance': 1 },
    'lognormal':
        { 'function': partial(np.random.lognormal, *[1, 1]),
          'mean': np.exp(1 + 1.0 / 2),
          'variance': (np.exp(1) - 1) * np.exp(2 + 1) }
}

results = {}
for dist_name, properties in distributions.items():
    for n in sample_sizes:
        dist_function = properties["function"]
        mu = properties["mean"]
        sigma = properties["variance"]
        samples = dist_function(size=(trials, n))
        means = samples.mean(axis=1).reshape(trials, 1)
        variances = samples.var(axis=1).reshape(trials, 1)

        if subproblem == 'c': # overwrite variances if part (c)
            variances = sigma * np.ones((trials, 1))

        for q in quantiles:
            if q == 'Z':
                scores = stats.norm.ppf([alpha / 2, 1 - alpha / 2])
            else:
                scores = stats.t.ppf([alpha / 2, 1 - alpha / 2], df=(n-1))
            lo = means + scores.min() / np.sqrt(variances / n)
            hi = means + scores.max() / np.sqrt(variances / n)
            res = np.logical_and(lo <= mu, mu <= hi).reshape(trials, 1)
            results[(dist_name, n, q)] = res.sum()


print()
print('{:^39}'.format('Problem 3({})'.format(subproblem)))
print('{:^39}'.format('Seed: {}'.format(seed)))
print('{:^39}'.format('Confidence Interval: {}%'.format(100 * (1 - alpha))))
print()
print('{:<2}|{:^14}|{:^21}'.format('  ', 'Distribution', 'Sample Sizes'))
print('--+--------------+--------------------')
print('  | {:>12} | '.format(''), end='')
for n in sample_sizes:
    print('{:>4d} '.format(n), end='')
print()

for quant in ['Z', 'T']:
    print('--+--------------+--------------------')
    for idx, dist_name in enumerate(distributions):
        quantile = quant if idx == int(len(distributions) / 2) else ' '
        print('{:<2}| {:<12} |'.format(quantile, dist_name), end='')
        for n in sample_sizes:
            print(' {:>4d}'.format(results[(dist_name, n, quant)]), end='')
        print()
print()
