from __future__ import print_function

from random import expovariate, seed

import pandas as pd
import numpy as np
from SimPy.Simulation import (Process, Resource, activate, hold, initialize,
                              now, release, request, simulate)


class Source(Process):

    def generate(self, number, meanTBA, server):
        for i in range(number):
            job = Job(name="Job%02d"%(i,))
            activate(job, job.visit(server=server))
            t = expovariate(1.0 / meanTBA)
            yield hold, self, t


class Job(Process):

    def visit(self, server):
        global time_data, jobs_data
        arrive = now()
        print("{:8.4f} {}: Arrived".format(now(), self.name))
        jobs_data.append((0 if not jobs_data else jobs_data[-1]) + 1)
        time_data.append(now())
        yield request, self, server
        wait = now() - arrive
        print("{:8.4f} {}: Waited {:8.4f}".format(now(), self.name, wait))
        tib = expovariate(1.0 / MODEL_MU)
        yield hold, self, tib
        yield release, self, server
        print("{:8.4f} {}: Finished".format(now(), self.name))
        jobs_data.append(jobs_data[-1] - 1)
        time_data.append(now())


MODEL_LAMBDA = 0.9
MODEL_MU = 1.0
MAX_JOBS = 10000
MAX_TIME = 500.0

time_data = []
jobs_data = []

initialize()
source = Source('Source')
server = Resource(name="Server00")
activate(source, source.generate(number=MAX_JOBS, meanTBA=1.0 / MODEL_LAMBDA,
                                 server=server), at=0.0)
simulate(until=MAX_TIME)
print("{:8.4f}: End of Simulation".format(now()))
jobs_data.append(jobs_data[-1])
time_data.append(now())

busy_data = []
start_time = 0
for idx in range(1, len(time_data)):
    if jobs_data[idx - 1] == 0 and jobs_data[idx] >= 1:
        start_time = time_data[idx]
    elif jobs_data[idx - 1] >= 1 and jobs_data[idx] == 0:
        end_time = time_data[idx]
        busy_data.append(end_time - start_time)
time_data = np.array(time_data)
jobs_data = np.array(jobs_data)
busy_data = np.array(busy_data or [0])
average = (np.diff(time_data) * jobs_data[:-1]).sum() / MAX_TIME
variance = (np.diff(time_data) * (jobs_data[:-1] - average) ** 2).sum() / MAX_TIME
print('avg_num: {}, var_num: {}, avg_busy: {}'.format(average, variance, busy_data.mean()))
