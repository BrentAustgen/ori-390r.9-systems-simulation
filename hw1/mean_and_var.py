import numpy as np

_mu = 1.0
#for _lambda in [0.3, 0.9, 1.1]:
for _lambda in [1.1]:
    _rho = _lambda / _mu
    filename = 'simulation-{}.log'.format(_lambda)
    print(filename)

    avg_results = []
    var_results = []
    busy_results = []
    with open(filename, 'r') as file_handle:
        for line in file_handle:
            avg_string, var_string, busy_string = line.split(',')
            avg_results.append(float(avg_string.split(':')[-1]))
            var_results.append(float(var_string.split(':')[-1]))
            busy_results.append(float(busy_string.split(':')[-1]))

    avg_results = np.array(avg_results)
    var_results = np.array(var_results)
    busy_results = np.array(busy_results)

    print('  expected: avg={}, var={}, busy={}'.format(_rho / (1 - _rho), _rho / (1 - _rho) ** 2, 1 / (_mu - _lambda)))
    for i in range(len(avg_results)):
        print('  result {}: avg={}, var={}, busy={}'.format(i, avg_results[i], var_results[i], busy_results[i]))
#    print('     total: avg={}, var={}'.format(avg_results.mean(), var_results.mean()))

