import time

import numpy as np

from objects import *
from utils import *

interarrival_pdf = lambda: np.random.exponential(20)
nodes_pdf = lambda: 2 ** np.random.randint(0, 5)
walltime_pdf = lambda: abs(np.random.normal(30, 10))

seed = 2

job_cfg = JobConfiguration(interarrival_pdf, nodes_pdf, walltime_pdf,
                           cutoff=1440, seed=seed)

cluster_cfg =  ClusterConfiguration(16, priority=lambda job: 1 / job.nodes)

sim = Simulation(cluster_cfg, job_cfg, name='sim0')
sim.run()

# https://nickcharlton.net/posts/drawing-animating-shapes-matplotlib.html
for job in sorted(sim.jobs, key=lambda x: x.arrival_time):
    print(job.name, job.nodes, job.walltime,
          job.arrival_time, job.allocation_time, job.completion_time)

plot_utilization(sim.node_availability)
