"""
https://stackoverflow.com/questions/48738371/simpy-requesting-multiple-nonspecific-resources-and-order-of-requests
"""


import simpy
import numpy as np


class ClusterConfiguration(object):
    def __init__(self, size, priority=None):
        self.size = size
        if priority is None:
            self.priority = lambda job: 1
        else:
            self.priority = priority


#class Cluster(simpy.PriorityResource):
class Cluster(simpy.PreemptiveResource):

    def __init__(self, env, cluster_cfg):
#        simpy.PriorityResource.__init__(self, env, capacity=cluster_cfg.size)
        simpy.PreemptiveResource.__init__(self, env, capacity=cluster_cfg.size)
        self.priority = cluster_cfg.priority
        self.env = env
    
    def allocate(self, job):
        reqs = [self.request(self.priority(job)) for _ in range(job.nodes)]
        job.allocation = simpy.AllOf(self.env, reqs)
        return job.allocation

    def relinquish(self, job):
        rels = [self.release(event) for event in job.allocation._events]
        return simpy.AllOf(self.env, rels)

    @property
    def nodes(self):
        return self.capacity


class JobConfiguration(object):

    def __init__(self, interarrival_pdf, nodes_pdf, walltime_pdf,
                 cutoff=None, seed=None):
        self.interarrival_pdf = interarrival_pdf
        self.nodes_pdf = nodes_pdf
        self.walltime_pdf = walltime_pdf

        self.cutoff = np.float('inf') if cutoff is None else cutoff

        self._seed = seed
        if not seed is None:
            np.random.seed(seed)

    def factory(self):
        i = 0
        arrival_time = 0
        interarrival_time = self.interarrival_pdf()
        while arrival_time + interarrival_time < self.cutoff:
            i += 1
            arrival_time += interarrival_time
            nodes = self.nodes_pdf()
            walltime = self.walltime_pdf()
            name = "Job{}".format(i)
            yield Job(arrival_time, nodes, walltime, name=name)
            interarrival_time = self.interarrival_pdf()


class Job(object):

    def __init__(self, arrival_time, nodes, walltime, name=None):
        self.arrival_time = arrival_time
        self.nodes = nodes
        self.walltime = walltime
        self.name = name

        # simulation internals
        self.allocation = None

        # simulation data
        self.allocation_time = None
        self.completion_time = None


class Simulation(simpy.Environment):

    def __init__(self, cluster_cfg, job_cfg, name=None):
        simpy.Environment.__init__(self)
        self.cluster = Cluster(self, cluster_cfg)

        self.node_availability = {}
        self.node_availability[self.now] = self.cluster.nodes - self.cluster.count

        self.jobs = list()
        self.job_factory = job_cfg.factory()
        for job in self.job_factory:
            self.process(self.arrival(job))

    def _setup_event_tracking(self):
        self.node_availability = {}

    def execute(self, job):
        print(job.name, self.cluster.nodes - self.cluster.count)
        yield self.cluster.allocate(job)
        self.node_availability[self.now] = self.cluster.nodes - self.cluster.count
        job.allocation_time = self.now
        yield self.timeout(job.walltime)
        job.completion_time = self.now
        yield self.cluster.relinquish(job)
        self.node_availability[self.now] = self.cluster.nodes - self.cluster.count

    def arrival(self, job):
        yield self.timeout(job.arrival_time)
        self.jobs.append(job)
        self.process(self.execute(job))
