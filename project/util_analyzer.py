import os
import argparse

import matplotlib.cm as colormap
import matplotlib.pyplot as plt
import pandas as pd


# Parse command-line arguments.
###############################################################################
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', required=True, type=str)
parser.add_argument('--dpi', default=96, type=int)
_args = parser.parse_args()
datafile = _args.input
dpi = _args.dpi

# build data
priorities = ['FIFO', 'LIFO', 'T-min', 'T-max',
              'S-min', 'S-max', 'TS-min', 'TS-max']
df = pd.read_csv(datafile, index_col=0)
jobgen = df['jobgen'].unique()[0]
cluster_size = 0
while 'P' + str(cluster_size + 1) in df.columns:
    cluster_size += 1
Pcols = ['P' + str(i) for i in range(cluster_size + 1)]
avgs = df.groupby('priority')[Pcols].mean()
avgs = avgs.loc[priorities, :]
sums = avgs.sum(axis=1)
norm_avgs = avgs.div(sums, axis=0) * 100
lsums = sums.tolist()

# Make a visualization of utility breakdown and time to process.
###############################################################################

# setup matplotlib
fig, axes = plt.subplots(2, sharex=True,
                         figsize=(960 / dpi, 720 / dpi), dpi=dpi)

# visualize utility breakdown in subplot 1
cmap = colormap.plasma
ax1 = norm_avgs[Pcols[::-1]].plot(kind='bar', stacked=True, cmap=cmap,
                                  ax=axes[0])
ax1.get_legend().remove()
ax1.set_ylabel('% Simulation Time')
ax1.set_ylim([0, 100])
norm = plt.Normalize(0, 64)
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])
cbar = plt.colorbar(sm, ax=ax1)
cbar.set_ticks(range(0, 65, 8))
cbar.set_ticklabels(range(64, -1, -8))
cbar.set_label('Node Utilization Legend', labelpad=10)

# visualize processing time in subplot 2
tmpdf = sums / 60 / 24 - 30
tmpdf = pd.concat([sums / 60 / 24 - tmpdf, tmpdf], axis=1)
tmpdf.columns = ['Queue Open', 'Queue Closed']
ax2 = tmpdf.plot(kind='bar', stacked=True, color=['black', 'white'],
                 edgecolor='black', ax=axes[1])
ax2.set_ylim([0, 50])
pos1 = ax1.get_position()
pos2 = ax2.get_position()
ax2.set_position([pos1.x0, pos2.y0, pos1.width, pos1.height])
for tick in ax2.get_xticklabels():
    tick.set_rotation(0)
ax2.legend(loc='upper right', bbox_to_anchor = (1.26, 1.02), frameon=False)
ax2.set_ylabel('Simulation Time (Days)')
ax2.set_xlabel('Policy')
ax2.yaxis.set_ticks(range(0, 55, 5))
ax2.yaxis.set_ticks(range(30, 55, 5), minor=True)
ax2.grid(which='minor', axis='y')

filename = 'util_breakdown_{}.png'.format(jobgen)
filename = os.path.join('doc', 'images', filename)
print('Saving utilization breakdown plot to {}.'.format(filename))
plt.savefig(filename, dpi=dpi)
plt.close()
