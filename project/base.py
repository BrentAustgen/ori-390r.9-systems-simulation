"""
This module supplies a core framework for developing calendar/event-based
simulation models in Python.
"""

from abc import ABC, abstractmethod


class Event:
    """
    An event object keeps track of how much time remains until an event occurs
    and the nature of the event via function, arguments, and keyword arguments.
    """

    def __init__(self, countdown, function, *args, **kwargs):
        self.countdown = countdown
        self._f = lambda: function(*args, **kwargs)

    def execute(self):
        """
        Allows the event transpire.
        """
        self._f()

    def advance(self, timedelta):
        """
        Reduces the countdown by the specified amount.
        """
        self.countdown -= timedelta


class Calendar:
    """
    A calendar object maintains a list of events yet to occur and a clock.
    """

    def __init__(self):
        self.clock = 0
        self._events = list()

    def add(self, event):
        """
        Adds the specified event to the calendar.
        """
        self._events.append(event)

    def remove(self, event):
        """
        Removes the specified event from the calendar.
        """
        self._events.remove(event)

    def execute(self):
        """
        Executes any events schedule for the current simulation clock time,
        then removes those events from the calendar.
        """
        while self.has_events_now():
            exec_events = [event for event in self._events
                           if event.countdown == 0]
            for event in exec_events:
                event.execute()
                self.remove(event)

    def advance(self, timedelta=None):
        """
        If timedelta is specified, the simulation clock is advanced by that
        duration, but only if that duration does not exceed the time until the
        next calendar event. If the duration exceeds that time, an error is
        raised.

        If timedelta is not specified, the simulation clock advances to the
        precise time of the next calendar event.
        """
        if not timedelta:
            timedelta = self.time_to_next_event()
            self.clock += timedelta
            for event in self._events:
                event.advance(timedelta)

        else:
            if timedelta <= self.time_to_next_event():
                self.clock += timedelta
                for event in self._events:
                    event.countdown -= timedelta
            else:
                msg = 'Specified time delta exceeds time to next event.'
                raise ValueError(msg)

    def time_to_next_event(self):
        """
        Returns the duration the simulation clock will be advanced if advance
        is advance is performed.
        """
        return min(self._events, key=lambda x: x.countdown).countdown

    def has_events(self):
        """
        Returns true is there are still events on the calendar.
        """
        return len(self._events) > 0

    def has_events_now(self):
        """
        Returns true if there are events on the calendar that are ready for
        execution at the present simulation clock time.
        """
        return len([event for event in self._events if event.countdown == 0]) > 0


class Simulation(ABC):
    """
    A simulation object provides a namespace for simulation model components.
    This object is deliberately abstract and meant to be subclassed.
    """

    def __init__(self):
        self.calendar = Calendar()

    @property
    def clock(self):
        """
        Getter for calendar's clock.
        """
        return self.calendar.clock

    @clock.setter
    def clock(self, clock):
        """
        Setter for calendar's clock.
        """
        self.calendar.clock = clock

    def schedule(self, event):
        """
        Adds the specified event to the calendar.
        """
        self.calendar.add(event)

    def is_active(self):
        """
        Returns true if there are still events on the calendar.
        """
        return self.calendar.has_events()

    @abstractmethod
    def run(self):
        """
        Method that runs the simulation.
        """
