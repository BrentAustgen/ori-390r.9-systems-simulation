"""
TODO:
Ultimately, these should all be part of the SchedulerSimulation class.
"""

import matplotlib.pyplot as plt
import numpy as np

factorial = np.math.factorial

def plot_utilization(data):
    _times, _levels = zip(*data)
    t0, l0 = _times[0], _levels[0]
    tf, lf = _times[-1], _levels[-1]
    times, levels = list(), list()

    times.append(t0)
    levels.extend([l0, l0])

    for time, level in zip(_times[1:-1], _levels[1:-1]):
        times.extend([time, time])
        levels.extend([level, level])
    times.append(tf)
    plt.plot(times, np.array(levels))
    plt.show()


class MMcQueue(object):

    def __init__(self, l, u, c):
        self._l = l # interarrival rate
        self._u = u # service rate
        self._c = c # number of servers

    @property
    def l(self):
        return self._l

    @property
    def u(self):
        return self._u

    @property
    def c(self):
        return self._c

    @property
    def p(self):
        return self.l / self.u

    @property
    def a(self):
        return self.p / self.c

    @property
    def Q(self):
        """Average queue length."""
        return self.P(0) * self.p ** self.c / factorial(self.c) *\
                self.a / (1 - self.a) ** 2

    @property
    def N(self):
        """Average number of jobs in system."""
        return self.Q + self.p

    @property
    def U(self):
        """Average utilization."""
        return self.a

    @property
    def T(self):
        """Average waiting time in system."""
        return self.W + 1 / self.u

    @property
    def W(self):
        """Average waiting time in queue."""
        return self.Q / self.l

    @property
    def P0(self):
        """Average proportion of time spent idle."""
        return 1 / (sum(self.p ** n / factorial(n)
                        for n in range(self.c)) +\
                    self.p ** self.c / factorial(self.c) / (1 - self.a))

    def P(self, i):
        """Average proportion of time spent with i servers busy."""
        return self.P0 * self.C(i)

    def C(self, i):
#        if i == 0:
#            C = 1
        if i <= self.c:
            C = self.p ** i / factorial(i)
        else:
            C = self.p ** i / factorial(self.c) / self.c ** (i - self.c)
        return C
