"""
This module is the meat and potatoes of the project. It implements many of
the abstract simulation framework components and applies them to the problem
of cluster job scheduling using non-preemptive fixed-priority scheduling
algorithms.
"""

from collections import deque, defaultdict

import numpy as np

from base import Simulation, Event


class SchedulerSimulation(Simulation):
    """
    SchedulerSimulation implements the abstract Simulation class.

    Within an object of this class, a simulation run for the cluster scheduling
    problem may performed, and data may be collected.
    """

    def __init__(self, cluster, jobs):
        # inheritance
        Simulation.__init__(self)
        # simulation attributes
        self.cluster = cluster
        self.jobs = deque(sorted(jobs, key=lambda job: job.arrival_time))
        self.queue = deque()
        # data tracking attributes
        self.job_idx = 0
        self.node_utilization = list()
        self.system_job_level = 0
        self.system_job_levels = list()
        self.queue_job_level = 0
        self.queue_job_levels = list()
        self.alloc_job_level = 0
        self.alloc_job_levels = list()

    def run(self, until=float('inf')):
        """
        This is the method that allows the simulation to run. The Simulation
        abstract base class requires that this method be defined.

        To initialize the simulation, this method introduces the first job to
        the system as an event.

        Following that, the method runs in a loop provided there are still
        events on the calendar. Inside that loop, there are two steps. First
        is a check to see if the next calendar event is in the future. If so,
        the simulation clock is advanced to that point. Second, another loop
        is performed -- while there are events to be executed at the current
        simulation clock time, run them then queue a job if possible. Repeat
        until there are no more events scheduled at the current simulation
        clock time.

        If the optional 'until' argument is specified, the simulation is
        run until that time is reached or the simulation naturally ends,
        whichever comes first. In the former case, the simulation state is
        retained, and simulation may continue if this method is again invoked.
        """
        # initialize the first event -- a job arrival
        if self.job_idx == 0 and self.job_idx < len(self.jobs):
            job = self.jobs[self.job_idx]
            self.job_idx += 1
            self.schedule(Event(job.arrival_time, self._introduce, job))

        # run simulation
        while self.is_active():

            # check simulation timeout criteria
            if self.clock + self.calendar.time_to_next_event() > until:
                self.register()
                self.calendar.advance(until - self.clock)
                break

            # continue simulation
            self.register()
            self.calendar.advance()
            while self.calendar.has_events_now():
                self.calendar.execute()
                if self.queue and self.queue[0].nodes <= self.cluster.nodes:
                    job = self.queue.popleft()
                    self.schedule(Event(0, self._allocate, job))

        self.register()

    def register(self):
        """
        Registers the present state of the simulation. This method should be
        invoked prior to every calendar advance.
        """
        node_util = self.cluster.size - self.cluster.nodes
        self.node_utilization.append((self.clock, node_util))
        self.system_job_levels.append((self.clock, self.system_job_level))
        self.queue_job_levels.append((self.clock, self.queue_job_level))
        self.alloc_job_levels.append((self.clock, self.alloc_job_level))

    def _allocate(self, job):
        """
        Allocates cluster resources to the given job, then schedules an future
        event to relinquish that allocation.
        """
        job.service_start_time = self.clock
        self.cluster.nodes -= job.nodes
        self.queue_job_level -= 1
        self.alloc_job_level += 1
        self.schedule(Event(job.walltime, self._relinquish, job))

    def _relinquish(self, job):
        """
        Relinquishes the specified job's hold on cluster resources at the end
        of its execution. After this, the job exits the system and no new
        events are scheduled for it.
        """
        job.service_end_time = self.clock
        self.cluster.nodes += job.nodes
        self.system_job_level -= 1
        self.alloc_job_level -= 1

    def _introduce(self, job):
        """
        Introduces the specified job to the system and puts it in queue.
        Additionally, the next job is scheduled as long as it exists.
        """
        # place the job in queue based on its priority
        for idx, other in enumerate(self.queue):
            if job.priority < other.priority:
                self.queue.insert(idx, job)
                break
        # otherwise, put the job in the back of the queue (this covers the
        # empty queue case as well)
        else:
            self.queue.append(job)

        self.system_job_level += 1
        self.queue_job_level += 1

        if self.job_idx < len(self.jobs):
            job = self.jobs[self.job_idx]
            self.job_idx += 1
            arrival_timedelta = job.arrival_time - self.clock
            self.schedule(Event(arrival_timedelta, self._introduce, job))

    def cluster_utilization_periods(self):
        """
        Computes the the periods and levels of node utilization.
        """
        periods, levels = list(), list()
        ta, la = 0, self.cluster.size
        for tb, lb in self.node_utilization:
            periods.append(tb - ta)
            levels.append(la / self.cluster.size)
            ta, la = tb, lb
        return periods, levels

    def queue_job_periods(self):
        """
        Computes the periods and levels of jobs in queue.
        """
        periods, levels = list(), list()
        ta, la = self.queue_job_levels[0]
        for tb, lb in self.queue_job_levels[1:]:
            periods.append(tb - ta)
            levels.append(la)
            ta, la = tb, lb
        return periods, levels

    def alloc_job_periods(self):
        """
        Computes the periods and levels of allocated jobs.
        """
        periods, levels = list(), list()
        ta, la = self.alloc_job_levels[0]
        for tb, lb in self.alloc_job_levels[1:]:
            periods.append(tb - ta)
            levels.append(la)
            ta, la = tb, lb
        return periods, levels

    def system_job_periods(self):
        """
        Computes the periods and levels of all jobs in system.
        """
        periods, levels = list(), list()
        ta, la = self.alloc_job_levels[0]
        for tb, lb in self.system_job_levels[1:]:
            periods.append(tb - ta)
            levels.append(la)
            ta, la = tb, lb
        return periods, levels

    def wait_times(self):
        """
        Returns a list of job wait times given in order of job arrival. The
        job wait time is the time a job spends waiting on a resource allocation
        after it has been submitted.
        """
        return [job.service_start_time - job.arrival_time
                for job in self.jobs
                if job.service_start_time is not None]

    def system_times(self):
        """
        Returns a list of job system times given in order of job arrival. The
        job system time is simply the time a job spends in the system between
        arrival and service completion.
        """
        return [job.service_end_time - job.arrival_time
                for job in self.jobs
                if job.service_end_time is not None]

    def system_metrics(self):
        """
        Returns a avg. utilization (U), avg. waiting time in queue (W), avg. time
        in system (T), avg. queue length (Lq), and avg. # of jobs in system (L).
        """
        results = {}
        results['U'] = np.dot(*self.cluster_utilization_periods()) / self.clock
        results['W'] = np.array(self.wait_times()).mean()
        results['T'] = np.array(self.system_times()).mean()
        results['Q'] = np.dot(*self.queue_job_periods()) / self.clock
        results['N'] = np.dot(*self.system_job_periods()) / self.clock
        results.update({'P' + str(i): v
                        for i, v in enumerate(self.utilization_breakdown())})
        return results

    def utilization_breakdown(self):
        """
        Returns a list whose length is the number of nodes in the system plus
        one. The value stored in index i is the proportion of the time the
        system has spent with exactly i nodes allocated.
        """
        util_dict = defaultdict(lambda: 0)
        events = self.node_utilization
        for i in range(len(events) - 1):
            util_dict[events[i][1]] += (events[i+1][0] - events[i][0])

        for key in util_dict.keys():
            if util_dict[key] == 0:
                util_dict.pop(key)
        util_list = [util_dict[level]
                     for level in range(self.cluster.size + 1)]
        return util_list


class Cluster:
    """
    The Cluster class stores information about a cluster in the context of
    a SchedulerSimulation. A cluster tracks its total of total nodes and its
    currently active nodes.
    """

    def __init__(self, nodes):
        # simulation attributes
        self.size = nodes
        self.nodes = nodes


class Job:
    """
    The Job class stores information about a single job in the context of
    a SchedulerSimulation. A job tracks its own arrival and service time (i.e.
    walltiem), the number of nodes it requires, its priority, and its name.
    """

    def __init__(self, arrival_time, nodes, walltime,
                 priority=lambda job: 1, name=None):
        # simulation attributes
        self.arrival_time = arrival_time
        self.nodes = nodes
        self.walltime = walltime
        self.priority = priority(self)
        self.name = name
        # data tracking attributes
        self.service_start_time = None
        self.service_end_time = None
