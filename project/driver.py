"""
Consider a scenario where the cluster is known NOT to be able to process jobs
as they arrive. That is, the resource is oversaturated by demand. To counteract
this, system administrators want to plan allow the cluster to "catch up" to the
jobs by disabling new jobs for a few days at the beginning of each month.
Ideally, that downtime should be as small as possible. To determine the best
policy for achieving this goal, the sysadmins simulate each policy where jobs
are allowed to be schedule for 30 days, roughly a month.

Let
  t1 = amount of time jobs are allowed to be scheduled
  T2 = amount of time system needs to process all jobs scheduled up to time t1
  m = approximate number of days in a month

Then, the statistic of interest is

  m - floor(t1 / T2 * m)

which is an estimator for the number of days out of 30 for which scheduling
new jobs should be disallowed to allow the cluster to catch up to demand.

In this scenario, we assume that users do not flood the cluster with jobs after
a downtime, i.e. the average interarrival time is not changing over time

An interesting multi-statistic, non-linear metric to compute is number of jobs
scheduled over a period divided by the amount of time required to process those
jobs.
"""

import os
from collections import deque
from itertools import chain, product
from multiprocessing import Pool
from copy import deepcopy

import numpy as np
import pandas as pd
import scipy.stats as stats
import seaborn as sns

from simulation import *
from analysis import *



def driver(jobgen=None, seed=None, clustersize=None):

    for var in [jobgen, seed, clustersize]:
        if var is None:
            raise ValueError(var)

    seeded_jobgen = jobgen(seed)

    cluster = Cluster(clustersize)

    timeout = 30 * 24 * 60 # 30 days in units of minutes

    jobs = list()
    clock = 0
    i = 0
    while True:
        iat, nodes, walltime = next(seeded_jobgen)
        clock = clock + iat 
        if clock >= timeout:
            break
        name = 'Job{}'.format(i)
        job = Job(clock, nodes, walltime, name=name)
        jobs.append(job)
        i += 1

    i = None
    priorities = {
        'FIFO':   lambda job:  i,
        'LIFO':   lambda job: -i,
        'S-min':  lambda job:  job.nodes,
        'S-max':  lambda job: -job.nodes,
        'T-min':  lambda job:  job.walltime,
        'T-max':  lambda job: -job.walltime,
        'TS-min': lambda job:  job.nodes * job.walltime,
        'TS-max': lambda job: -job.nodes * job.walltime
    }

    reslist = list()
    for priority, fpriority in priorities.items():
        print(seed, priority)
        copyjobs = deepcopy(jobs)
        for i, job in enumerate(copyjobs, start=1):
            job.priority = priorities[priority](job)

        sim = SchedulerSimulation(cluster, copyjobs)
        sim.run(until=timeout)
        t1 = sim.clock
        sim.run()
        t2 = sim.clock

        # get all the goodies
        res = sim.system_metrics()
        # report function parameters
        res['priority'] = priority
        res['seed'] = seed
        res['jobgen'] = jobgen.__name__
        # job and simulation end times
        res['T1'], res['T2'] = t1, t2
        # ratio of T1 to T2
        res['R'] = t1 / t2
        # number of days the cluster can be allowed to run
        res['D'] = np.floor(t1 / t2 * 30)
        # max wait/system times
        res['W_max'] = np.array(sim.wait_times()).max()
        res['T_max'] = np.array(sim.system_times()).max()

        reslist.append(res)
    return reslist


def jobgen1(seed=None):
    if not seed is None:
        np.random.seed(seed)
    l = (2 ** 7 - 1) / 7
    IAT = stats.expon(scale=l)
    EXP = stats.randint(low=0, high=6+1)
    SVT = stats.norm(loc=64, scale=16)
    while True:
        iat = IAT.rvs()
        nct = 2 ** EXP.rvs()
        svt = SVT.rvs()
        while svt < 0: # very small probability of this occurring
            svt = SVT.rvs()
        yield iat, nct, svt

def jobgen2(seed=None):
    if not seed is None:
        np.random.seed(seed)
    IAT = stats.expon(scale=65.0/2)
    NCT = stats.randint(low=1, high=64+1)
    SVT = stats.uniform(loc=0, scale=128)
    while True:
        iat = IAT.rvs()
        nct = NCT.rvs()
        svt = SVT.rvs()
        yield iat, nct, svt

def jobgen3(seed=None):
    if not seed is None:
        np.random.seed(seed)
    l = (2 ** 7 - 1) / 7
    IAT = stats.expon(scale=l)
    EXP = stats.randint(low=0, high=6+1)
    SVT = stats.uniform(loc=0, scale=128)
    while True:
        iat = IAT.rvs()
        nct = 2 ** EXP.rvs()
        svt = SVT.rvs()
        while svt < 0: # very small probability of this occurring
            svt = SVT.rvs()
        yield iat, nct, svt


# Run a bunch of simulations.
###############################################################################
cols = ['priority', 'seed', 'jobgen',
        'U', 'Q', 'N', 'W', 'T',
        'W_max', 'T_max', 'T1', 'T2', 'R', 'D'] +\
       ['P' + str(i) for i in range(64 + 1)]
seeds = list(range(0, 10000))
clustersizes = [64]
generators=[jobgen1, jobgen2, jobgen3]

filename = 'project_data.csv'
if not os.path.exists(filename):
    params = product(
        generators,
        seeds,
        clustersizes
    )
    params = sorted(params, key=lambda x: x[1])
    with Pool(processes=16) as pool:
        rowgroups = pool.starmap(driver, params)
    rows = list(chain(*rowgroups))
    df = pd.DataFrame(rows, columns=cols)
    df.sort_values(inplace=True, axis=0, by=['priority', 'seed'])
    df.reset_index(inplace=True, drop=True)
    print('Writing results to \'{}\'.'.format(filename))
    df.to_csv(filename)
else:
    print('WARNING: File \'{}\' already exists. Stopping.'.format(filename))
