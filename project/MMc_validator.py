import os
from collections import deque
from itertools import product
from multiprocessing import Pool

import pandas as pd
import numpy as np
import scipy.stats as stats
from tqdm import tqdm

from simulation import *
from analysis import *

ls = [0.25, 0.5, 0.75]
us = [1.0]
cs = [1, 2, 4]
num_jobs = 1000

def do_sim(l, u, c, seed):
    print(l, u, c, seed)
    np.random.seed(seed)
    IAT = stats.expon(loc=0, scale=1/l)
    SVT = stats.expon(loc=0, scale=1/u)
    jobs = deque()
    clock = 0
    cluster = Cluster(c)

    for i in range(num_jobs):
        iat = IAT.rvs()
        svt = SVT.rvs()
        clock += iat
        name = 'Job{}'.format(i)
        jobs.append(Job(clock, 1, svt, name=name))

    sim = SchedulerSimulation(cluster, jobs)
    sim.run()

    res = sim.system_metrics()
    new_row = {'l': l, 'u': u, 'c': c, **res}
    return new_row

"""
P_{0} - probability of no busy servers
Ubar - mean utilization
Qbar - average number in queue
Nbar - average number in system
Wbar - average time in queue
Tbar - average time in system
"""

# empirical data
datafile = 'validation.csv'
if not os.path.exists(datafile):
    with Pool(processes=4) as pool:
        rows = pool.starmap(do_sim, product(ls, us, cs, range(1000)))

    emp = pd.DataFrame(columns=['l', 'u', 'c', 'U', 'Q', 'N', 'W', 'T'])
    for row in rows:
        emp = emp.append(row, ignore_index=True)
    emp.to_csv('validation.csv')
else:
    emp = pd.read_csv('validation.csv', index_col=0)
print(emp)

# theoretical data
thr = pd.DataFrame(columns=['l', 'u', 'c', 'U', 'Q', 'N', 'W', 'T'])
for l, u, c in product(ls, us, cs):
    mmc = MMcQueue(l, u, c)
    new_row = {attr: getattr(mmc, attr) for attr in thr.columns}
    thr = thr.append(new_row, ignore_index=True)

# make a pretty table
emp['c'] = emp['c'].astype(int)
thr['c'] = thr['c'].astype(int)
avg = emp.groupby(['l', 'u', 'c']).mean()
var = emp.groupby(['l', 'u', 'c']).var()
thr = thr.groupby(['l', 'u', 'c']).mean()
avg.columns = [('$' + col + '$', 'avg.') for col in avg.columns]
var.columns = [('$' + col + '$', 'var.') for col in var.columns]
thr.columns = [('$' + col + '$', 'theoretical') for col in thr.columns]

master = pd.concat([avg, var, thr], axis=1)
master = master[[('$U$', 'avg.'),  ('$U$', 'var.'),  ('$U$', 'theoretical'),
                 ('$Q$', 'avg.'),  ('$Q$', 'var.'),  ('$Q$', 'theoretical'),
                 ('$N$', 'avg.'),  ('$N$', 'var.'),  ('$N$', 'theoretical'),
                 ('$W$', 'avg.'),  ('$W$', 'var.'),  ('$W$', 'theoretical'),
                 ('$T$', 'avg.'),  ('$T$', 'var.'),  ('$T$', 'theoretical')]]

master.index.names = ['$\lambda$', '$\mu$', '$c$']
master.columns = pd.MultiIndex.from_tuples(master.columns)
master.columns.names = ['stat.', None]
master = master.groupby(level=[0, 1], axis=1, sort=False).sum()
master = master.stack(level=0)
master = master[['avg.', 'var.', 'theoretical']]

alpha = 0.05
t = stats.t(df=len(master)-1).ppf(1 - alpha / 2)
lo = master['avg.'] - t * np.sqrt(master['var.'] / len(master))
hi = master['avg.'] + t * np.sqrt(master['var.'] / len(master))
# all theoretical stats are covered by empirical CIs using t-dist quantiles
master['95\% CI lo'] = lo
master['95\% CI hi'] = hi

master.drop('var.', axis=1, inplace=True)
master.rename({'avg.': 'empirical'}, axis=1, inplace=True)

master = master[['theoretical', 'empirical', '95\% CI lo', '95\% CI hi']]

# In the written state, some manual changes are necessary.
# 1. The index column headers and column headers are collapsed into
#    a single row.
# 2. \cmidrule{3-8} are added to separate data into sections.
# 3. Reorder stacked variables U, Lq, L, Wq, W
master.to_latex('validation.tex', escape=False)
