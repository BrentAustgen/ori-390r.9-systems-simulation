from collections import deque
from copy import deepcopy

from simulation import *


cluster = Cluster(4)

# by default, priority is FIFO
jobs = deque([
    Job(0, 1, 4, name='JobA'),
    Job(1, 2, 3, name='JobB'),
    Job(2, 4, 2, name='JobC'),
    Job(3, 3, 1, name='JobD'),
    Job(4, 3, 2, name='JobE'),
    Job(5, 1, 2, name='JobF'),
])

print('FIFO')
jobs1 = deepcopy(jobs)
for i, job in enumerate(jobs1):
    job.priority = i # FIFO
sim = SchedulerSimulation(cluster, jobs1)
sim.run()
for job in sorted(sim.jobs, key=lambda job: job.service_start_time):
    print(job.name, job.service_start_time, job.service_end_time)

print('LIFO')
jobs2 = deepcopy(jobs)
for i, job in enumerate(jobs2):
    job.priority = len(jobs) - i
sim = SchedulerSimulation(cluster, jobs2)
sim.run()
for job in sorted(sim.jobs, key=lambda job: job.service_start_time):
    print(job.name, job.service_start_time, job.service_end_time)

print('S-min')
jobs3 = deepcopy(jobs)
for i, job in enumerate(jobs3):
    job.priority = job.nodes
sim = SchedulerSimulation(cluster, jobs3)
sim.run()
for job in sorted(sim.jobs, key=lambda job: job.service_start_time):
    print(job.name, job.service_start_time, job.service_end_time)

print('S-max')
jobs4 = deepcopy(jobs)
for i, job in enumerate(jobs4):
    job.priority = -job.nodes
sim = SchedulerSimulation(cluster, jobs4)
sim.run()
for job in sorted(sim.jobs, key=lambda job: job.service_start_time):
    print(job.name, job.service_start_time, job.service_end_time)

print('T-min')
jobs5 = deepcopy(jobs)
for i, job in enumerate(jobs5):
    job.priority = job.walltime
sim = SchedulerSimulation(cluster, jobs5)
sim.run()
for job in sorted(sim.jobs, key=lambda job: job.service_start_time):
    print(job.name, job.service_start_time, job.service_end_time)

print('T-max')
jobs6 = deepcopy(jobs)
for i, job in enumerate(jobs6):
    job.priority = -job.walltime
sim = SchedulerSimulation(cluster, jobs6)
sim.run()
for job in sorted(sim.jobs, key=lambda job: job.service_start_time):
    print(job.name, job.service_start_time, job.service_end_time)

print('TS-min')
jobs7 = deepcopy(jobs)
for i, job in enumerate(jobs7):
    job.priority = job.nodes * job.walltime
sim = SchedulerSimulation(cluster, jobs7)
sim.run()
for job in sorted(sim.jobs, key=lambda job: job.service_start_time):
    print(job.name, job.service_start_time, job.service_end_time)

print('TS-max')
jobs8 = deepcopy(jobs)
for i, job in enumerate(jobs8):
    job.priority = -job.nodes * job.walltime
sim = SchedulerSimulation(cluster, jobs8)
sim.run()
for job in sorted(sim.jobs, key=lambda job: job.service_start_time):
    print(job.name, job.service_start_time, job.service_end_time)
