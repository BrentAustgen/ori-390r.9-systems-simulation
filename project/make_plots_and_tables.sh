#!/bin/bash

for filename in project*.csv; do
    echo $filename
    python3 dist_analyzer.py -i $filename
    python3 util_analyzer.py -i $filename
    for metric in U W W_max D; do
        python3 var_reduce.py -i $filename -m $metric -S 10000 -s 1000
        python3 comparison2.py -i $filename -m $metric -S 10000 -a 0.05
    done
done
