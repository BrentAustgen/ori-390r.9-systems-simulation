import argparse
import os
from itertools import permutations, combinations

import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as colormap
import scipy.stats as stats


# Parse command-line arguments.
###############################################################################
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', required=True, type=str)
parser.add_argument('-m', '--metric', required=True, type=str)
parser.add_argument('-s', '--samples-to-use', required=True, type=int)
parser.add_argument('-S', '--samples-per-policy', required=True, type=int)
parser.add_argument('--dpi', default=96, type=int)
_args = parser.parse_args()
samples_per_policy = _args.samples_per_policy
samples_to_use = _args.samples_to_use
datafile = _args.input
metric = _args.metric
dpi = _args.dpi


def latexify_label(metric):
    metric = str(metric)
    result = None
    if metric == 'priority':
        result = 'Priority'
    elif '_' in metric:
        _main, _sub = metric.split('_')
        result = ''.join(['$', _main, '_{', _sub, '}$'])
    else:
        result = ''.join(['$\overline{', metric, '}$'])
    return result


# Build the DataFrame.
###############################################################################
df = pd.read_csv(datafile, index_col=0)
df['priority'] = df['priority'].astype('category')
jobgen = df['jobgen'].unique()[0]
zdf = pd.DataFrame(columns=['Method', 'Policy Pair', 'Z'])

idxs = list()
cats = df['priority'].cat.categories
for i, _ in enumerate(cats):
    subidxs = range(samples_per_policy * i + samples_to_use * i,
                    samples_per_policy * i + samples_to_use * (i + 1))
    idxs.extend(list(subidxs))
groups = dict((key, grp) for (key, grp) in df.loc[idxs, :].groupby('priority'))
for p1, p2 in combinations(cats, r=2):
    Z = groups[p1][metric].values - groups[p2][metric].values
    mini_zdf = pd.DataFrame(columns=zdf.columns)
    mini_zdf['Z'] = Z
    mini_zdf['Method'] = ['Naïve'] * len(Z)
    mini_zdf['Policy Pair'] = [(p1, p2)] * len(Z)
    zdf = zdf.append(mini_zdf)

idxs = list()
for i, _ in enumerate(cats):
    subidxs = range(samples_per_policy * i,
                    samples_per_policy * i + samples_to_use)
    idxs.extend(list(subidxs))
groups = dict((key, grp) for (key, grp) in df.loc[idxs, :].groupby('priority'))
for p1, p2 in combinations(cats, r=2):
    Z = groups[p1][metric].values - groups[p2][metric].values
    mini_zdf = pd.DataFrame(columns=zdf.columns)
    mini_zdf['Z'] = Z
    mini_zdf['Method'] = ['CRN'] * len(Z)
    mini_zdf['Policy Pair'] = [(p1, p2)] * len(Z)
    zdf = zdf.append(mini_zdf)

# Make the comparative bar chart.
###############################################################################
mdf = zdf.groupby(['Policy Pair', 'Method']).var().unstack('Method')
mdf.columns = ['CRN', 'Naïve']
mdf = mdf[['Naïve', 'CRN']]
fig, ax = plt.subplots(figsize=(900 / dpi, 600 / dpi))
mdf.plot.barh(ax=ax)
xlabel = ' '.join(['Var($Z$)',
                   'for Comparisons of {}'.format(latexify_label(metric))])
plt.xlabel(xlabel)
plt.subplots_adjust(left=0.20, right=0.97, bottom=0.11, top=0.95,
                    wspace=0.20, hspace=0.20)
filename = os.path.join('doc', 'images',
                        'var_reduce_{}_{}.png'.format(jobgen, metric))
print('Saving variance reduction plot to {}.'.format(filename))
plt.savefig(filename, dpi=dpi)
