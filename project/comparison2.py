import argparse
import os
from itertools import permutations, combinations

import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as colormap
import scipy.stats as stats


# Parse command-line arguments.
###############################################################################
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', required=True, type=str)
parser.add_argument('-m', '--metric', required=True, type=str)
parser.add_argument('--dpi', default=96, type=int)
parser.add_argument('-S', '--samples-per-policy', required=True, type=int)
parser.add_argument('-a', '--alpha', default=0.05, type=float)
_args = parser.parse_args()
samples_per_policy = _args.samples_per_policy
datafile = _args.input
metric = _args.metric
dpi = _args.dpi
alpha = _args.alpha


# Compare naïve simulation to simulation with common random numbers (CRN).
###############################################################################

# set comparison parameters (metric, policies to compare, joint threshold)
policies = ['FIFO', 'LIFO', 'S-min', 'S-max',
            'T-min', 'T-max', 'TS-min', 'TS-max']

def latexify_label(metric):
    metric = str(metric)
    result = None
    if metric == 'priority':
        result = 'Priority'
    elif '_' in metric:
        _main, _sub = metric.split('_')
        result = ''.join(['$', _main, '_{', _sub, '}$'])
    else:
        result = ''.join(['$\overline{', metric, '}$'])
    return result


# Build the DataFrame.
###############################################################################
df = pd.read_csv(datafile, index_col=0)
jobgen = df['jobgen'].unique()[0]
df = df[['priority', metric]]
df['priority'] = df['priority'].astype('category')
groups = dict(list(df.groupby('priority')))

poolvar = sum(group[metric].var() for _, group in groups.items())
sqrtterm = np.sqrt(2 / samples_per_policy)
K = len(policies)
beta = 2 * alpha / (K * (K - 1))
t = stats.t.ppf(1 - beta / 2, df=samples_per_policy-len(policies))
crit = poolvar * sqrtterm * t

zdf = pd.DataFrame(columns=['Policy Pair', 'Z'])
for p1, p2 in combinations(df['priority'].cat.categories, r=2):
    Z = groups[p1][metric].values - groups[p2][metric].values
    mini_zdf = pd.DataFrame(columns=zdf.columns)
    mini_zdf['Z'] = Z
    mini_zdf['Policy Pair'] = [(p1, p2)] * len(Z)
    zdf = zdf.append(mini_zdf)


# Make a heatmap showing differences among policies.
###############################################################################
K = len(policies)
beta = 2 * alpha / (K * (K - 1))
t = stats.t.ppf(1 - beta / 2, df=samples_per_policy-len(policies))

z_los = pd.DataFrame(columns=policies, index=policies).fillna(0)
z_his = pd.DataFrame(columns=policies, index=policies).fillna(0)
z_averages = pd.DataFrame(columns=policies, index=policies).fillna(0)
groups = dict(list(zdf.groupby('Policy Pair')))

for pi, pj in permutations(policies, r=2):
    if pi == pj:
        continue
    if (pi, pj) in groups:
        m = groups[(pi, pj)]['Z'].mean()
    else:
        m = groups[(pj, pi)]['Z'].mean() * -1
    lo = m - crit
    hi = m + crit
    z_los.loc[pi, pj] = lo
    z_his.loc[pi, pj] = hi
    z_averages.loc[pi, pj] = m

cmap=colormap.seismic_r
cbar_label = ' '.join(['$\overline{Z}$ Legend',
                       'for Comparisons of {}'.format(latexify_label(metric))])
cbar_kws = {'label': cbar_label}
ax = sns.heatmap(data=z_averages, cmap=cmap, cbar_kws=cbar_kws)
ax.set_aspect('equal')
plt.xticks(rotation=45)
plt.yticks(rotation=45)

pairs = list(combinations(policies, r=2))
z_cis = pd.concat([z_averages.stack(),
                   z_los.stack(),
                   z_his.stack(),
                   np.logical_or(z_his.stack()< 0, z_los.stack() > 0)],
                  axis=1)

z_cis.columns = [r'$\overline{Z}^{ij}$',
                 r'95\% lo', r'95\% hi', 'Holds']
z_cis.index.names = ['Policy $i$', 'Policy $j$']
z_cis.reset_index(inplace=True)
z_cis = z_cis[z_cis[['Policy $i$', 'Policy $j$']].apply(tuple, axis=1).isin(pairs)]
z_cis.reset_index(inplace=True, drop=True)
filename = 'compare_{}_{}_{}.tex'.format(jobgen, metric, str(alpha))
filename = os.path.join('doc', 'tables', filename)

float_formats = {
    'U': lambda x: '{:8.6f}'.format(x),
    'W': lambda x: '{:10.3f}'.format(x),
    'W_max': lambda x: '{:10.3f}'.format(x), 
    'D': lambda x: '{:8.6f}'.format(x)
}

z_cis.to_latex(filename, index=False, float_format=float_formats[metric],
               escape=False)

# Report if any of the CIs do not cover 0 (and thus do not hold).
###############################################################################
joint_coverage = np.all(z_cis['Holds'])

filename = 'compare_{}_{}_{}.png'.format(jobgen, metric, str(alpha))
filename = os.path.join('doc', 'images', filename)

if not joint_coverage:
    filename = filename.split('.png')[0] + '-FAIL.png'

print('Saving comparison plot to {}.'.format(filename))
plt.savefig(filename, dpi=dpi)
