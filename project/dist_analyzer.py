import os
import argparse

import matplotlib.cm as colormap
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


# Parse command-line arguments.
###############################################################################
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', required=True, type=str)
parser.add_argument('--dpi', default=96, type=int)
_args = parser.parse_args()
datafile = _args.input
dpi = _args.dpi

# specify parameters 
metrics = ['U', 'W', 'W_max', 'D']
priorities = ['FIFO', 'LIFO', 'T-min', 'T-max',
              'S-min', 'S-max', 'TS-min', 'TS-max']

def latexify_label(metric):
    metric = str(metric)
    result = None
    if metric == 'priority':
        result = 'Priority'
    elif '_' in metric:
        _main, _sub = metric.split('_')
        result = ''.join(['$', _main, '_{', _sub, '}$'])
    else:
        result = ''.join(['$\overline{', metric, '}$'])
    return result

# build data
df = pd.read_csv(datafile, index_col=0)
jobgen = df['jobgen'].unique()[0]
maindf = df[['priority'] + metrics]

# Make a pairplot of the metric data.
###############################################################################

fancylabels = list(map(latexify_label, metrics))
plot_kws = {"s": 10,
            "linewidth": 0.25}
pairgrid = sns.pairplot(maindf, hue='priority', plot_kws=plot_kws)
for ax, label in zip(pairgrid.axes[-1], fancylabels):
    ax.set_xlabel(label)
for axes_row, label in zip(pairgrid.axes, fancylabels):
    axes_row[0].set_ylabel(label)
legend = pairgrid.fig.get_children()[-1]
legend.set_title('Policy')
pairgrid.fig.set_size_inches(1200 / dpi, 720 / dpi)
filename = os.path.join('doc', 'images', 'corr_{}.png'.format(jobgen))
print('Saving metric pairplot to {}.'.format(filename))
plt.savefig(filename, dpi=dpi)
plt.close()


# Make an individual KDE plot for each of the metrics of interest.
###############################################################################
for col in maindf.columns:
    if col == 'priority':
        continue
    fig, ax = plt.subplots(figsize=(720 / dpi, 480 / dpi), dpi=dpi)
    groups = dict(list(maindf[['priority', col]].groupby('priority')))
    for priority, group in groups.items():
        sns.kdeplot(data=group[col], ax=ax, label=priority)
    legend = ax.get_legend()
    ax.set_xlabel(latexify_label(col))
    ax.set_ylabel('Approximate PDFs')
    ax.legend(loc='right', bbox_to_anchor=(1.23, 0.50), frameon=False)
    fig.subplots_adjust(left=0.15, right=0.8)
    filename = os.path.join('doc', 'images', 'kde_{}_{}.png'.format(jobgen, col))
    print('Saving KDE plot to {}.'.format(filename))
    plt.savefig(filename, dpi=dpi)
    plt.close()

"""
# TODO: see if this is useful
for col in maindf.columns:
    if col == 'Priority':
        continue
    sns.boxenplot(data=maindf, x='Priority', y=col, hue='Priority')
    plt.show()
"""
