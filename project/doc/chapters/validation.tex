\chapter{Model Validation}\label{chap:validation}

\section{M/M/n Queue}

In our model, we allow all the variables -- job inter-arrival times, walltimes, and node requirements -- come from general distributions. To validate our model, we leverage this flexibility by allowing inter-arrival times and walltimes to come from independent exponential distributions. \\

The next most complicating factor is that our model of the system requires jobs be processes by some number of nodes concurrently. However, let us assume that jobs only ever require exactly one node for processing and that they are processed in order of arrival (i.e. the queue has a standard arrival-based prioritization policy). Then, if we assume the cluster is comprised of exactly $n$ nodes, we can use our model to emulate an M/M/$n$ queue. \\

The M/M/$n$ queue, and especially its M/M/1 queue subclass, are well-studied, and we have elegant closed-form formulas of their primary characteristics. To that end, we credit Dr. János Sztrik for his text \cite{Sztrik2012} in which he derives and details these characteristics for several basic queues including M/M/$n$. In a limited capacity, we may  validate the behavior of our model by directly comparing its behavior as an M/M/$n$ queue to the theoretical behavior. Specifically, we are compare mean utilization $\overline{U}$, mean queue length $\overline{Q}$, mean number of jobs in the system $\overline{N}$, mean time waiting in queue $\overline{W}$, and average time in the system $\overline{T}$. For notational convenience, we also are interested in proportion of system idle time $P_{0}$. Assuming rate $\lambda$ for our exponential inter-arrivals and rate $\mu$ for our exponential walltimes, let $\rho = \tfrac{\lambda}{\mu}$ and $\alpha = \tfrac{\rho}{n}$. Then we have the following definitions.

\begin{align}[H]
P_{0} &= \Bigg( \sum_{k=0}^{n-1} \frac{\rho^{k}}{k!} + \frac{\rho^{n}}{n!} \frac{1}{1-\alpha} \Bigg)^{-1} \\
\overline{U} &= \alpha \\
\overline{Q} &= P_{0} \frac{\rho^{n}}{n!} \frac{\alpha}{(1 - \alpha)^{2}} \\
\overline{N} &= \overline{Q} + \rho \\
\overline{W} &= P_{0} \frac{\rho^{n}}{n!} \frac{1}{(1 - \alpha)^{2}n\mu} \\
\overline{T} &= \overline{W} + \frac{1}{\mu}
\end{align}

To validate our model, we ran 1000 simulations each of 9 differently parameterized M/M/$n$ queues, comparing our empirical values of the above characteristics to their theoretical values. Using empirical means and variances to develop 95\% confidence intervals, we found that for all five characteristics and all nine parameterizations of the M/M/$n$ queue, the CI covers the theoretical value. These results, presented in \autoref{tab:mmn_emulation}, give us confidence in our implementation.

\section{Complexity of Job Processing}\label{sect:job_processing}

As we mentioned in \autoref{chap:implementation}, our first failed attempt passed the validation technique from the previous section. When we introduced jobs requiring multiple nodes and queue priorities based on other factors than arrival time, our initial model failed. Accordingly, we developed an auxiliary validation scenario to pinpoint the issues and test the behavior of our reworked implementation. The scenario is comprised of six jobs with deterministic qualities. The scenario is deliberately simple so we may easily compare the model's behavior to our expectations for it. To describe jobs, we introduce notation -- a job $j$ requires $n_j$ nodes for processing, arrives at time $a_j$, and has walltime $w_j$. Using this notation, we describe this auxiliary scenario in \autoref{tab:complex_validation_scenario}.

\begin{table}[H]
\centering
\caption{A Simple Scenario for Validating Complex Job Processing}
\label{tab:complex_validation_scenario}
\vspace{0.5\baselineskip}
\begin{tabular}{rrrr}
    \toprule
    $j$ & $n_j$ & $a_j$ & $w_j$ \\
    \midrule
    A & 1 & 0 & 4 \\
    B & 2 & 1 & 3 \\
    C & 4 & 2 & 2 \\
    D & 3 & 3 & 1 \\
    E & 3 & 4 & 2 \\
    F & 1 & 5 & 2 \\
    \bottomrule
\end{tabular}
\end{table}

We ran this set of jobs through simulation eight times, one each for eight different job prioritization policies, and manually validated the results. These eight policies are actually four pairs of idologically opposite policies. FIFO (first-in, first-out) is the traditional queue policy where first arrivals are the first to be serviced. In contrast, there is the opposite LIFO (last-in, first-out) policy which is analogous to a stack (vs. a queue). For the remaining policies, we use `S' and `T' to mean space and time, respectively. The S-min and S-max policies prioritize a job based on the number of nodes it requires, the T-min and T-max policies prioritize by walltime, and the TS-min and TS-max policies prioritize by the product of nodes and walltime. The main focus of our analysis in \autoref{chap:analysis} is the comparison of these policies, and that is why we elect to validate their behaviors here using simple examples. We present our validated observations in \autoref{tab:complex_validation_findings} and leave the argument of their correctness as an exercise for the reader.

\begin{table}[H]
\centering
\caption{Results of Complex Job Processing Validation Scenario}
\label{tab:complex_validation_findings}
\vspace{0.5\baselineskip}
\begin{tabular}{llrrrrrr}
    \toprule
    Policy & First-Out of Queue & \multicolumn{6}{l}{Processing Order} \\
    \midrule
      FIFO &                           First-In & A & B & C & D & E & F \\
      LIFO &                            Last-In & A & B & E & F & D & C \\
     S-min &                        Least Nodes & A & B & D & E & F & C \\
     S-max &                         Most Nodes & A & B & C & D & E & F \\
     T-min &                  Shortest Walltime & A & B & D & C & E & F \\
     T-max &                   Longest Walltime & A & B & C & E & F & D \\
    TS-min & Smallest Walltime-by-Nodes Product & A & B & D & E & F & C \\
    TS-max &  Largest Walltime-by-Nodes Product & A & B & C & E & D & F \\
    \bottomrule
\end{tabular}
\end{table}


\begin{table}
\centering
\caption{Means and CIs Obtained through Emulating M/M/n Queue}
\label{tab:mmn_emulation}
\vspace{0.5\baselineskip}
\footnotesize
\begin{tabular}{llllrrrr}
\toprule
$\lambda$ & $\mu$ & $n$ & stat.&  theoretical &  empirical &  95\% CI lo &  95\% CI hi \\
\midrule
0.25 & 1.0 & 1 & $\overline{U}$ &     0.250000 &   0.250453 &    0.247058 &    0.253849 \\
     &     &   & $\overline{Q}$ &     0.083333 &   0.083829 &    0.079927 &    0.087730 \\
     &     &   & $\overline{N}$ &     0.333333 &   0.334282 &    0.327704 &    0.340860 \\
     &     &   & $\overline{W}$ &     0.333333 &   0.334425 &    0.320127 &    0.348723 \\
     &     &   & $\overline{T}$ &     1.333333 &   1.334900 &    1.314471 &    1.355329 \\
\cmidrule{3-8}
     &     & 2 & $\overline{U}$ &     0.125000 &   0.125233 &    0.123535 &    0.126931 \\
     &     &   & $\overline{Q}$ &     0.003968 &   0.003996 &    0.003551 &    0.004441 \\
     &     &   & $\overline{N}$ &     0.253968 &   0.254462 &    0.250902 &    0.258022 \\
     &     &   & $\overline{W}$ &     0.015873 &   0.015937 &    0.014191 &    0.017683 \\
     &     &   & $\overline{T}$ &     1.015873 &   1.016412 &    1.006321 &    1.026504 \\
\cmidrule{3-8}
     &     & 4 & $\overline{U}$ &     0.062500 &   0.062617 &    0.061768 &    0.063465 \\
     &     &   & $\overline{Q}$ &     0.000009 &   0.000013 &   -0.000005 &    0.000030 \\
     &     &   & $\overline{N}$ &     0.250009 &   0.250479 &    0.247082 &    0.253876 \\
     &     &   & $\overline{W}$ &     0.000036 &   0.000050 &   -0.000020 &    0.000120 \\
     &     &   & $\overline{T}$ &     1.000036 &   1.000526 &    0.990956 &    1.010095 \\
\midrule
0.50 & 1.0 & 1 & $\overline{U}$ &     0.500000 &   0.500597 &    0.493820 &    0.507375 \\
     &     &   & $\overline{Q}$ &     0.500000 &   0.501852 &    0.474616 &    0.529089 \\
     &     &   & $\overline{N}$ &     1.000000 &   1.002450 &    0.969994 &    1.034905 \\
     &     &   & $\overline{W}$ &     1.000000 &   1.000956 &    0.951185 &    1.050727 \\
     &     &   & $\overline{T}$ &     2.000000 &   2.001432 &    1.945985 &    2.056878 \\
\cmidrule{3-8}
     &     & 2 & $\overline{U}$ &     0.250000 &   0.250382 &    0.246990 &    0.253774 \\
     &     &   & $\overline{Q}$ &     0.033333 &   0.033710 &    0.031117 &    0.036303 \\
     &     &   & $\overline{N}$ &     0.533333 &   0.534474 &    0.526077 &    0.542871 \\
     &     &   & $\overline{W}$ &     0.066667 &   0.067207 &    0.062258 &    0.072156 \\
     &     &   & $\overline{T}$ &     1.066667 &   1.067683 &    1.055484 &    1.079882 \\
\cmidrule{3-8}
     &     & 4 & $\overline{U}$ &     0.125000 &   0.125193 &    0.123497 &    0.126889 \\
     &     &   & $\overline{Q}$ &     0.000258 &   0.000282 &    0.000156 &    0.000408 \\
     &     &   & $\overline{N}$ &     0.500258 &   0.501053 &    0.494249 &    0.507857 \\
     &     &   & $\overline{W}$ &     0.000516 &   0.000561 &    0.000311 &    0.000811 \\
     &     &   & $\overline{T}$ &     1.000516 &   1.001036 &    0.991444 &    1.010629 \\
\midrule
0.75 & 1.0 & 1 & $\overline{U}$ &     0.750000 &   0.749416 &    0.739351 &    0.759481 \\
     &     &   & $\overline{Q}$ &     2.250000 &   2.237688 &    2.034521 &    2.440856 \\
     &     &   & $\overline{N}$ &     3.000000 &   2.987104 &    2.776555 &    3.197654 \\
     &     &   & $\overline{W}$ &     3.000000 &   2.975710 &    2.721373 &    3.230046 \\
     &     &   & $\overline{T}$ &     4.000000 &   3.976185 &    3.716637 &    4.235734 \\
\cmidrule{3-8}
     &     & 2 & $\overline{U}$ &     0.375000 &   0.375415 &    0.370332 &    0.380498 \\
     &     &   & $\overline{Q}$ &     0.122727 &   0.123912 &    0.115314 &    0.132510 \\
     &     &   & $\overline{N}$ &     0.872727 &   0.874742 &    0.857869 &    0.891615 \\
     &     &   & $\overline{W}$ &     0.163636 &   0.164727 &    0.153937 &    0.175517 \\
     &     &   & $\overline{T}$ &     1.163636 &   1.165202 &    1.147932 &    1.182472 \\
\cmidrule{3-8}
     &     & 4 & $\overline{U}$ &     0.187500 &   0.187719 &    0.185178 &    0.190261 \\
     &     &   & $\overline{Q}$ &     0.001768 &   0.001857 &    0.001402 &    0.002312 \\
     &     &   & $\overline{N}$ &     0.751768 &   0.752734 &    0.742440 &    0.763028 \\
     &     &   & $\overline{W}$ &     0.002357 &   0.002464 &    0.001864 &    0.003064 \\
     &     &   & $\overline{T}$ &     1.002357 &   1.002940 &    0.993249 &    1.012631 \\
\bottomrule
\end{tabular}
\end{table}
