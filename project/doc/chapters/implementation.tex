\chapter{Model Implementation}\label{chap:implementation}

\section{First Attempt}

Our first (failed) implementation revolved around the SimPy package for Python. SimPy is a process-oriented simulation framework in which users may conveniently define processes using generator functions and define objects using primitive data structures or classes. SimPy provides a variety of off-the-shelf simulation components and a host of abstract classes for defining custom simulation components. As a disclaimer, we found SimPy to be a powerful simulation tool overall, and our only criticism is superficial. \\

After implementing the model in SimPy, we validated it using the methods described in \autoref{chap:validation} that simulate a scheduler strictly under the condition that each job is allocated exactly one node. It was only when we started simulating multiple node allocations that we observed an issue -- our model would progress through some amount of a simulation run then lock up. After some debugging, we realized an off-the-shelf \texttt{PriorityResource} object we had incorporated was allocating nodes one-at-a-time as they became available instead of all-at-once to the first job in queue. Effectively, we had allowed a race condition to occur in our implementation. Consider the following scenario that highlights the issue. In this scenario, jobs are labeled as X-Y where X is a unique identifier for the job and Y is the number of nodes it requires. \\

\begin{enumerate}
    \item[] $t=0$: Begin simulation on a 4-node cluster.
    \item[] $t=1$: Job A-2 enters queue and is immediately allocated 2 nodes.
    \item[] $t=2$: Job B-2 enters queue and is immediately allocated 2 nodes.
    \item[] $t=3$: Job C-4 enters queue and waits.
    \item[] $t=4$: Job A-2 terminates. Job C-4 is allocated its 2 nodes and waits for more resources.
    \item[] $t=5$: A prioritized job D-4 enters queue ahead of C-4 and waits.
    \item[] $t=6$: Job B-2 terminates. Job D-4 is allocated its 2 nodes and waits for more resources.
\end{enumerate}

At this point, both C-4 and D-4 have been allocated 2 of the 4 nodes they require. Neither may execute in this state, nor may they take nodes from the other. The \texttt{PriorityResource} object is incapable of resolving this situation, and with no nodes left available, the simulation stalls. After reading through the SimPy documentation, we determined that we were left with two options -- either extend SimPy by creating a custom resource object that performs all-at-once allocations or pursue other paths. \\

\section{Custom Implementation}

Ultimately, we abandoned SimPy for this project and developed our own calendar/event-based simulation framework in Python, then leveraged it for our specific application. Our core simulation framework and scheduler simulation that builds upon it are presented in \autoref{appendix:corecode} and \autoref{appendix:simcode}, respectively. The core framework provides abstract \texttt{Simulation}, \texttt{Calendar}, and \texttt{Event} classes that provide mechanisms for handling the passage of time and event scheduling. In the simulation code, the \texttt{SchedulerSimulation} class incorporates these mechanisms and extends them with events specific to our application and methods for post-processing simulation data. \\

As was mentioned in \autoref{chap:description}, in our implementation we isolate variability from the system.  The system behavior itself is deterministic, and it realizes variability exclusively through its input -- a set of jobs to process. Factors such as who submits a job and the priority policy are all built into a job prior to it entering the system. In implementation, the system does not actually need to be knowledgeable of these factors since we are only considering fixed-priority policies. By embedding the prioritization policy in the system input, we are afforded the luxury of implementing and maintaining a single general system. Moreover, since all variability is built into input, this allows us to isolate and synchronize the generation of random variates for purposes such as variance reduction (see \autoref{sect:variancereduction}). The one other input is the number of nodes belonging to the cluster; however, this quantity is deterministic for all intents and purposes. \\

In our implementation, we model three types of events dubbed `introduce', `allocate', and 'relinquish'. The first introduces jobs into the system and the last two control the assignment of nodes to jobs. The simulation starts when a main run method is invoked. The simulation pseudocode is given in \autoref{algorithm:simulation}.

\begin{algorithm}
    \caption{Scheduler Simulation Pseudocode}\label{algorithm:simulation}
    \begin{algorithmic}[1]
        \Procedure{run}{set of jobs $J$, number of cluster nodes $N$}
        \State initialize set of jobs in queue $Q \gets \emptyset$
        \State initialize simulation time $t \gets 0$
        \State initialize number of idle nodes $n \gets N$
        \State initialize an empty calendar of events
        \If{$J \ne \emptyset$}
            \State let $j \gets$ job in $J$ with soonest arrival time
            \State let $a \gets$ arrival time of job $j$
            \State schedule \Call{introduce}{$j$} event for time $a$ on calendar
        \EndIf
        \While{calendar contains an event}
            \State let $t \gets$ time of next soonest event on calendar
            \While{{calendar contains an event scheduled for time $t$}}
                \State let \Call{event}{$\cdot$} $\gets$ event scheduled for time $t$ on calendar
                \State execute \Call{event}{$\cdot$}
            \EndWhile
        \EndWhile
        \EndProcedure

        \State

        \Procedure{introduce}{job $j$}
            \State let $J \gets J - \{j\}$
            \State let $Q \gets Q \cup \{j\}$
            \If{$J \ne \emptyset$}
                \State let $j \gets$ job in $J$ with soonest arrival time
                \State let $a \gets$ arrival time of job $j$
                \State schedule \Call{introduce}{$j$} event for time $a$ on calendar
            \EndIf
        \EndProcedure
        
        \State
        
        \Procedure{allocate}{job $j$}
            \State let $Q \gets Q - \{j\}$
            \State let $n_j$ be the number of nodes required by job $j$
            \State let $s$ be the walltime of job $j$
            \State let $n \gets n - n_j$
            \State schedule \Call{relinquish}{$j$} event for time $t + s$ on calendar
        \EndProcedure

        \State

        \Procedure{relinquish}{job $j$}
            \State let $n_j$ be the number of nodes required by job $j$
            \State let $n \gets n + n_j$
            \If{jobs in queue}
                \State let $j \gets$ job in $Q$ with highest priority
                \State let $n_j \gets$ number of nodes job $j$ requires
                \If{$n \ge n_j$}
                    \State schedule \Call{allocate}{$j$} event for time $t$ on calendar
                \EndIf
            \EndIf
        \EndProcedure

    \end{algorithmic}
\end{algorithm}
