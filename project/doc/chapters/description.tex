\chapter{Model Description}\label{chap:description}

For our research, we develop a queueing model for cluster simulations. The number of possible cluster configurations is intractably large, so we make a number of simplifying assumptions. \\

First, we assume that all jobs are \textit{rigid}. In the taxonomy of job types (see \autoref{table:job_type_taxonomy}), this means that the number of nodes allocated to a job is constant and determined by the user at time of submission. This is the most common type of job and also the most simple to model.

\begin{table}[H]
\centering
\caption{The Taxonomy of Job Types \cite{Gupta2014}}
\label{table:job_type_taxonomy}
\vspace{0.5\baselineskip}
\begin{tabular}{ccc}
	\toprule
    Who Decides & \multicolumn{2}{c}{When Is It Decided} \\
    \cmidrule{2-3}
	            & At Submission & During Execution \\
	\midrule
	User        & Rigid         & Evolving \\
	Scheduler   & Moldable      & Malleable \\
	\bottomrule
\end{tabular}
\end{table}

Moreover, we assume the job walltime (i.e. service time) is also determined by the user at submission, and that a job's execution spans its entire walltime. Note that in practice, walltime is typically a user-defined upper limit on execution time, and jobs often naturally terminate before reaching the walltime limit. This assumption helps simplifies input definitions and the analysis of cluster metrics such as utilization. Also, we assume the submission times (i.e. arrival times) and job priorities are attributes of the jobs and not the cluster. \\

Regarding the cluster, we assume all nodes are identical, belong to the same pool, and have 100\% uptime. The number of nodes in the cluster is fixed. From the cluster's perspective, jobs are constants realized at time of submission. When the scheduler allocates nodes to a job, those nodes are allocated concurrently. Similarly, when the job has completed, the cluster repossesses all allocated nodes concurrently. As a conceptual tool, we consider that a single $n$-node job is the same as $n$ 1-node jobs that arrive as a batch and must also be processed as a batch. Though this is an obvious system constraint, it was an implementation struggle and is explained further in \autoref{chap:implementation}. \\

Ordinarily, we think of simple queue systems as having variability in two components -- jobs have random inter-arrival times and servers have random service times. The inter-arrival and service times in our model are both still random; however, the service times in our model are tied to the jobs and not the servers. Consequently, if we conceptually isolate the system (cluster) from its inputs (jobs), the system itself is deterministic and variability is realized exclusively in the inputs. Again, this is inconsequential in modeling, but it matters in implementation.