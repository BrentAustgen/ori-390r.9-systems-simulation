\chapter{Policy Analysis}\label{chap:analysis}

\section{Overview}

We have thoroughly described and validated our implementation, and we are ready to put it to the test. We consider a scenario in the introduction -- a directory and a system administrator have competing ideas about optimizing cluster usage. The director wishes to have the best possible utilization, and the system administrator prefers to minimize wait time on behalf of the cluster users he serves. The cluster is already established and has 64 identical nodes, and the distributions of the jobs parameters are known or at least well-approximated. \\

One issue with the cluster is that, under non-preemptive fixed-priority scheduling policies, it cannot be used to the fullest capacity unless it is saturated with job submissions. That is, if the per-node arrival rate to per-node service rate ratio is low, say 50\%, then the system will be 50\% utilized regardless of the policy. At higher levels of job saturation (but still less than 100\%), the batch processing restriction becomes prohibitive thereby causing a bottleneck and causing the cluster service to lag behind its demand. We hypothesize that the level at which the cluster becomes saturated is dependent on the job priority policy. \\

We design our experiment as follows. Considering that a month is roughly 30 days, we would like to know the maximum number of whole days $D$ out of 30 we can allow job submissions to the cluster such that the remaining $30 - D$ days provide a sufficient cooldown period during for the cluster to process jobs. In the cooldown period, the queue is disabled, so no new jobs can enter. We emulate this, we allow the simulation clock to run for $T_{1} = 30$ days with the queue active and permit the simulation to terminate when the last job is completed at time $T_{2}$. We assume that $T_{1}$ and $T_{2}$ are in the neighborhood of $D$ and 30 such that $\tfrac{T_{1}}{T_{2}}$ is a reasonable estimator of $\tfrac{D}{30}$. \\

Under these conditions, we judge the eight different priority policies described in \autoref{sect:job_processing} based on three quality of service criteria -- average utilization $\overline{U}$, average time in queue $\overline{W}$, and the maximum time in queue $W_{max}$. Additionally, we assess the average number of active days $\overline{D}$ described above estimated by $\lfloor\tfrac{30 T_{1}}{T_{2}}\rfloor$. \\

\section{Nature of Jobs}

In our analysis, we consider three different types of jobs. These types differ in their randomness, but we structure the randomness such that the following equation holds true.

\begin{equation}
\text{avg.\ nodes\ required\ *\ avg. inter-arrival\ rate\ =\ size\ of\ cluster\ /\ avg.\ walltime} \\
\end{equation}

Type I jobs have walltimes distributed normally as $\mathcal{N}(64, 16)$. In case a random variate pulled from this distribution is negative, it is discarded and replaced. Each job requires $2^{X}$ nodes where $X \sim \mathcal{U}(0 ,6)$, discrete and inclusive at both ends (such that the maximum job size demands the entire cluster). To satisfy the formula above, we define the inter-arrival rate $\lambda = \big(\frac{1}{7} \sum_{x=0}^{6}{ 2^{x}}\big)^{-1}$, the inverse of the mean number of required nodes, and let inter-arrival times be distributed as $exp(\lambda)$. We do not have empirical data to bolster confidence in these parameters. However, exponential inter-arrival times and normal service times are typical in many applications. Our rationale for the number of nodes being $2^X$ is that many software applications executed on clusters are tied to 2-D and 3-D physical objects. For example, a cube could be divided into 4-by-4-by-4 sub-cubes. Then, a user might think to request 64 nodes and have the application map nodes 1-to-1 to the sub-cubes such that each node is only responsible for the computations inside its sub-cube. \\

We develop the other two types of jobs to check if the system behaves differently when subjected to them, not necessarily because we believe they are a good representation of the real world. Type II jobs have walltimes distributed uniformly as $\mathcal{U}(0, 128)$, continuous. The number of nodes required is distributed uniformly as $\mathcal{U}(1, 64)$, discrete. And lastly, we define an inter-arrival rate $\lambda = \frac{65}{2}$ and let inter-arrival times be distributed exponentially as $exp(\lambda)$.  Type III jobs are a cross-over of Type I and Type II. Type III jobs have walltimes distributed like Type II jobs and inter-arrival times and node requirements distributed like Type I jobs.


\section{Variance Reduction}\label{sect:variancereduction}

Prior to starting several thousand simulation runs, we first wondered what benefit we might realize from using common random numbers. Since our analysis involves multiple comparison procedures, it would be ideal to minimize the variance of differences between pairs of policies.

To make this determination, we performed 1000 simulation runs using each of the eight policies using unsynchronized streams of random numbers. We think of this data as having been obtained through `naïve' means. Then we ran another 1000 simulations runs for each of the policies using synchronized streams. For both methods and each of the 28 (8 choose 2) pairs of policies, we computed differences $Z_{k} = U_{k,i} - U_{k,j}$ for each pair $(i, j)$ of policies. Finally, we computed the variance of those differences for comparison. Our results are illustrated in \autoref{fig:var_reduce_jobgen1_U}.

\begin{figure}[H]
    \centering
    \caption{Variance Reduction in Differences of $\overline{U}$ for Type I Jobs}
    \label{fig:var_reduce_jobgen1_U}
    \includegraphics[width=\linewidth]{images/var_reduce_jobgen1_U}
\end{figure}

In this figure, we observe that using common random numbers does indeed reduce the variance in the comparison of all policy pairs to varying degrees. Do note that here we only illustrate the variance in comparisons of $\overline{U}$ for Type I jobs. The complete series of illustrations for all job types and metrics of interest can be found in \autoref{appendix:varreduce}. There we see that variance reduction via common random numbers is effective in all cases. \\

\section{Comparison of Policies Subject to Type I Jobs}\label{sect:comparison}

Since common random numbers were an effective technique for reducing variance, we elected to leverage this at scale. To make comparisons, we start by collecting data from 10000 simulation runs of the system processing Type I jobs using FIFO policy. We then repeated this for the other seven policies so that, in total, we obtained data from 80000 simulation runs for Type I jobs. We present visualizations of the relationships between pairs of metrics in \autoref{fig:corr_jobgen1}. \\

\begin{landscape}
	\begin{figure}
		\centering
		\caption{Relationships between Pairs of Metrics Differentiated by Policy (Type I Jobs)}
		\label{fig:corr_jobgen1}
		\includegraphics[width=\linewidth]{images/corr_jobgen1}
	\end{figure}
\end{landscape}

This type of visualization has a couple of shortcomings. First, due to the size of our data, samples from similarly performing policies are obscured by means of overlap. Second, the plots on the diagonal show a kernel density estimation (KDE) of the empirical probability density functions. These provide valuable insight for the `continuous' data, but the KDE is a poor illustration for $\overline{D}$ which is discrete and should be represented by a probability \textit{mass} function. Unfortunately, our attempts to remedy this latter issue were of no use. \\

The disparate clusters of utilization (left-most column) are a remarkable feature of this data. Most of the policies permit the cluster to be around 70\% utilized on average. But there are two policies -- S-max and TS-max -- that perform exceptionally well with average utilization exceeding 90\%. Those policies appear to have mean waiting times a bit longer than most other policies and maximum waiting times a bit shorter than most other policies. Moreover, S-max and TS-max have visually similar distributions across all four metrics, and they have relatively large variances for mean and maximum waiting times. Recall that for Type I jobs, there are equal probabilities of the job requiring 1, 2, 4, 8, 16, 32, or 64 nodes and the walltimes are normally distributed. So TS-max will still almost always prioritize 64-node jobs over 32-node jobs, not much different that just the S-max policy. That being said, we are not surprised this policies behave so similarly. \\

FIFO is another interesting policy. It has the shortest maximum waiting time, and a mean waiting time that looks to be about in the middle of all policies. Lastly, of the policies clustered around 70\% average utilization, FIFO leads the group. \\

Having observed this visualization of the data, we would like to make a statement regarding the ranking of polices. To that end, we employ the following multiple comparisons procedure (MCP) based on Bonferroni corrections \cite{Montgomery2008}.

\begin{enumerate}
	\item[0.] Input: $r$ systems, $n$ samples $X_{1}^{i}, \ldots, X_{n}^{i}$ from each system $i = 1, \ldots, r$, joint confidence level $1 - \alpha$.
	\item[1.] For each system $i$, compute the mean $\overline{X}^{i}$ and variance $(S_{n}^{i})^{2}$.
	\item[2.] Compute pairwise differences $Z^{ij} = X^{i} - X^{j}$, for each pair $(i, j)$ of systems. Note that the pair ordering does not matter, and there are $\tbinom{r}{2}$ unordered pairs.
	\item[3.] Compute the pooled variance $S_{pool}^{2} = \sum_{i=1}^{r}{(S_{n}^{i})^{2}}$.
	\item[4.] For each pair of systems, construct a confidence interval $\big[\overline{Z}^{ij} \pm \sqrt{\tfrac{2}{n}} t_{n-r,1-\beta/2} S_{pool}\big]$ where $\beta = \tfrac{\alpha}{\binom{r}{2}}$ is the Bonferroni correction required for a \textit{joint} confidence level of $1 - \alpha$.
    \item[5.] Check that none of the confidence intervals straddle zero.
\end{enumerate}

Regarding the last step, we cannot develop a ranking of systems using this method if any one confidence interval straddles zero because that is indicative of two systems having no statistically significant difference. We can still develop a ranking, but not one that is backed by this statistically rigorous procedure. \\

We elect to use a threshold of $\alpha = 0.05$ to compare our $r = 8$ prioritization policies for which we have $n = 10000$ samples each. We start by applying this procedure to compare of systems in the $\overline{U}$ metric. For 8 policies, there are 28 unordered pairs of policies to consider. Our mean, variance, and confidence interval computations for the $\overline{U}$ metric are shown in \autoref{tab:compare_jobgen1_U_0.05}. \\

The variances for differences in this metric are small; consequently, none of the confidence intervals straddle zero. We conclude all eight policies are significantly different with a confidence level of 95\% and so proceed to ranking them. This ranking can be inferred from our illustration of the $\overline{Z}^{ij}$ in \autoref{fig:compare_jobgen_1_U_0.05}. The underlying matrix is skew-symmetric (i.e., $\overline{Z}^{ij} = -\overline{Z}^{ji}$, $\overline{Z}^{ii} = 0$ for all $i,j$). In this illustration, blue squares indicate the average metric of the row policy is larger than that of the column policy, and red indicates the opposite. Darker colors correspond to larger differences. In this illustration, we see the S-max policy marginally outperforms TS-max and dominates all other policies in average utilization. TS-max is the next-best policy followed by FIFO. These realizations confirm the hypotheses we formed from analyzing \autoref{fig:corr_jobgen1}. \\

\afterpage{
\begin{table}[t!]
	\centering
	\footnotesize
	\caption{MCP Confidence Intervals for the $\overline{U}$ Metric (Type I Jobs)}
	\label{tab:compare_jobgen1_U_0.05}
	\vspace{0.5\baselineskip}
	\input{tables/compare_jobgen1_U_0.05}
\end{table}

\begin{figure}[h!]
	\centering
	\caption{Comparisons of $\overline{Z}^{ij}$ for the $\overline{U}$ Metric (Type I Jobs)}
	\label{fig:compare_jobgen_1_U_0.05}
	\includegraphics[width=.75\linewidth]{{images/compare_jobgen1_U_0.05}.png}
\end{figure}
}

\pagebreak
\pagebreak

We proceed to apply this same MCP with $\overline{W}$ as the metric. What we find, however, is that a few large variances cause the pooled variance to be prohibitively large for forming confidence intervals that do not straddle zero (see \autoref{tab:compare_jobgen1_W_0.05}). Remedying this problem would require orders of magnitude more samples to reduce the variance or the introduction of a much better MCP. We present an illustration (see \autoref{fig:compare_jobgen_1_W_0.05}) of the relative differences in $\overline{W}$ but with the caveat that the MCP does not provide a justification for ranking the policies in this metric. Also, in contrast to $\overline{U}$, smaller values of $\overline{W}$ are preferred, so the `optimal rows' are red instead of blue. We can clearly see that S-min and TS-min are the best, just not in the statistically significant way we had hoped. \\

Let us also briefly comment on the other two metrics. Our analysis of $W_{max}$ was impaired in the same way as it was for $\overline{W}$ -- pooled variance is too high. And for $\overline{D}$, the MCP \textit{almost} provides a grounds for ranking, except that a small number of confidence intervals straddle zero. We can eliminate this problem by considering only subset of competitive policies; however, there is really no need since the rankings of policies in the $\overline{D}$ metric are the same as in the $\overline{U}$ metric. Again looking back at \autoref{fig:corr_jobgen1}, we can see there is a strong positive correlation between these two metrics, so this is an unsurprising result. \\

As a final note, the comprehensive set of MCP confidence interval tables and metric comparison charts is provided in \autoref{appendix:comparison}. This set also includes Type II and Type III jobs. \\

\afterpage{
	\begin{table}[t!]
		\centering
		\footnotesize
		\caption{MCP Confidence Intervals for the $\overline{W}$ Metric (Type I Jobs)}
		\label{tab:compare_jobgen1_W_0.05}
		\vspace{0.5\baselineskip}
		\input{tables/compare_jobgen1_W_0.05}
	\end{table}
	
	\begin{figure}[h!]
		\centering
		\caption{Comparisons of $\overline{Z}^{ij}$ for the $\overline{W}$ Metric (Type I Jobs)}
		\label{fig:compare_jobgen_1_W_0.05}
		\includegraphics[width=.75\linewidth]{{images/compare_jobgen1_W_0.05-FAIL}.png}
	\end{figure}
}

\clearpage
\pagebreak

\section{Comparison of Policies Subject to Type II and III Jobs}\label{sect:comparison}

To conclude analysis, we briefly discuss differences in behavior when the policies are subjected to other types of jobs. We repeat the process of performing 10000 simulation runs per policy for Type II and Type III jobs and provide similar pairwise relationship illustrations in \autoref{fig:corr_jobgen2} and \autoref{fig:corr_jobgen3}, respectively. \\

Applying the MCP to these sets of data, we saw similar results. The procedure provides a justification for ranking by $\overline{U}$, but not by $\overline{W}$ or $W_{max}$. For Type II jobs, some confidence intervals pertaining to the $\overline{D}$ metric still straddle zero, but for Type III jobs, none straddle zero. These results are all shown in detail in \autoref{appendix:comparison}. \\

For Type II jobs, we observe a much tighter grouping in utilization across policies. S-max stills wins out with roughly 78\% mean utilization, but the worst policy is not far behind at just over 72\%. For mean waiting time, we notice an interesting trend -- shorter waiting times are associated with smaller variance in waiting time. For maximum waiting time, FIFO is in a league of its own. While all other policies have an average maximum waiting time of over 60000 minutes, FIFO sits a bit under 20000. However, FIFO also has the worst mean utilization. Again, we observe an undeniable and intuitive relationship between $\overline{D}$ and $\overline{U}$. \\

For Type III jobs, the metrics are much more similar to those for Type I jobs than to those for Type II. The biggest difference in mean utilization for Type III jobs (as compared to Type I), is the wider margin between S-max and TS-max, the two best-performing policies. Conceptually, this makes sense. Because Type I jobs have normally distributed walltimes, it is a rare occurrence for TS-max to prioritize 64-node jobs over 32-node jobs, so it behaves similarly to S-max (that is agnostic to walltime). But for Type III jobs which have uniformly distributed walltimes, TS-max more frequently prioritize 32-node jobs over 64-node jobs which should make its behavior more distinct. We again see that S-max and TS-max have relatively wide distributions for both $\overline{W}$ and $W_{max}$. FIFO, for its third time, achieves the shortest maximum waiting time by a wide margin. And the strong positive correlation between $\overline{D}$ and $\overline{U}$ yet again prevails.

\begin{landscape}
	\begin{figure}[H]
		\centering
		\caption{Relationships between Pairs of Metrics Differentiated by Policy (Type II Jobs)}
		\label{fig:corr_jobgen2}
		\includegraphics[width=\linewidth]{images/corr_jobgen2}
	\end{figure}
\end{landscape}

\begin{landscape}
	\begin{figure}[H]
		\centering
		\caption{Relationships between Pairs of Metrics Differentiated by Policy (Type III Jobs)}
		\label{fig:corr_jobgen3}
		\includegraphics[width=\linewidth]{images/corr_jobgen3}
	\end{figure}
\end{landscape}

