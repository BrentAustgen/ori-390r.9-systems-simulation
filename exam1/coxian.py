import sys

import matplotlib.pyplot as plt
import numpy as np

N = 10

p = np.array([0.6, 0.9, 0.2])
q = 1 - p
mu = [1, 2, 3]
n = len(mu)

# based on consecutive coin flips
times = []
for _ in range(N):
    print("NEW VARIATE")
    t = 0
    for i in range(n):
        E = np.random.exponential(scale=mu[i])
        print("adding {}".format(E))
        t += E
        U = np.random.uniform()
        if U < q[i]:
            break
    print("OUTPUT {}".format(t))
    times.append(t)

times = np.array(times)

plt.plot(sorted(times), np.array(range(N)) / N, 'b-')


# based on convolution method
bins = np.cumsum([q[i] * np.product(p[0:i]) for i in range(n)])
bins = [0, *bins, 1]

times = []
for _ in range(N):
    Y = sum(w(i) * np.random.exponential(scale=mu[i]) for i in range(n))
    times.append(Y)

plt.plot(sorted(times), np.array(range(N)) / N, 'r--')
plt.show()
