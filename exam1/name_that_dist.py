import numpy as np
import matplotlib.pyplot as plt
from collections import Counter

N = 10000

u = np.linspace(0, 1, N + 1)[1:]
alpha = -1.0

# ceiling or floor ???
x = np.floor(np.log(u) / alpha)
ct_x = Counter(x)
print(ct_x)

plt.plot(u, np.ceil(np.log(u) / alpha), 'k--')
plt.show()

domain = np.array(list(ct_x.keys()))
probs = np.array(list(ct_x.values())) / N

plt.subplot(1, 3, 1)
plt.plot(domain, probs, 'o')

f = lambda x: np.exp(alpha * x) * (1 - np.exp(alpha))
p = 1 - np.exp(alpha)
g = lambda x: p * (1 - p) ** x

plt.subplot(1, 3, 2)
plt.plot(domain, list(map(f, domain)), 'rx')

plt.subplot(1, 3, 3)
plt.plot(domain, list(map(g, domain)), 'b^')

plt.show()
