import matplotlib.pyplot as plt
import numpy as np

N = 10000
a = 0
b = 2

alpha = 1.0

@np.vectorize
def F(X):
    return 1 - np.exp(-alpha * X)

@np.vectorize
def F_INV(U):
    return np.log(1 - U) / -alpha

@np.vectorize
def G_INV(Y):
    """
    DEFINITION:
      G(y) = (F(y) - F(a)) / (F(b) - F(a))
      => F(y) = G(y) * (F(b) - F(a)) + F(a)
      => y = F^-1(G(y) * (F(b) - F(a)) + F(a))
    INVERT: y -> G^-1(y), G(y) -> y
      => G^-1(y) = F^-1(y * (F(b) - F(a)) + F(a))
    """
    return F_INV(Y * (F(b) - F(a)) + F(a))

@np.vectorize
def G_AR(X):
    """
    This works because G is linear.
    """
    if a <= X <= b:
        return X
    else:
        return np.nan

U = np.linspace(0, 1, N + 1)[:-1]
U = np.random.uniform(size=N)

X = F_INV(U)
plt.subplot(3, 1, 1)
plt.xlim([0, 5])
plt.ylim([0, 1])
plt.title("X variates")
plt.plot(sorted(X), np.array(range(len(X))) / len(X))

Y = G_INV(U)
plt.subplot(3, 1, 2)
plt.xlim([0, 5])
plt.ylim([0, 1])
plt.title("Y variates from inversion method")
plt.plot(sorted(Y), np.array(range(len(Y))) / len(Y))

Y = G_AR(X)
Y = Y[np.logical_not(np.isnan(Y))]
efficiency = len(Y) / N
plt.subplot(3, 1, 3)
plt.xlim([0, 5])
plt.ylim([0, 1])
plt.title("Y variates from AR method (efficiency: {})".format(efficiency))
plt.plot(sorted(Y), np.array(range(len(Y))) / len(Y))


plt.subplots_adjust(hspace=1.0)
plt.show()
